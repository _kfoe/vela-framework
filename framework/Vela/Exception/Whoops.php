<?php declare (strict_types = 1);

namespace Vela\Exception;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Vela\Container\Container;
use Vela\Exception\ConvertExceptionToResponse;
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

/**
 * Whoops class
 */
class Whoops
{
    /**
     * Config instance
     *
     * @var \Vela\Config\Config
     */
    protected $config;

    /**
     * Container instance
     *
     * @var \Vela\Container\Container
     */
    protected $container;

    /**
     * Logger instance
     *
     * @var \Monolog\Logger
     */
    protected $logger;

    /**
     * Whoops instance
     *
     * @var \Whoops\Run
     */
    protected $whoops;

    /**
     * Construct
     *
     * @param \Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->whoops = new Run;

        $this->logger = new Logger('VelaFramework');

        $this->registerMonologHandler();

        $this->registerWhoopsHandler();
    }

    /**
     * Register Monolog handler
     *
     * @return void
     */
    private function registerMonologHandler()
    {
        $handler = new \Monolog\Handler\StreamHandler(
            basePath("storage/log/log.html")
        );

        $lineFormatter = new \Monolog\Formatter\LineFormatter();
        $lineFormatter->includeStacktraces();

        $handler->setFormatter($lineFormatter);

        $this->logger->pushHandler(
            $handler
        );
    }

    /**
     * Register Whoops handlers
     *
     * @return void
     */
    private function registerWhoopsHandler()
    {
        $self = $this;

        if (true === $this->container->get('config')->application['error_handler']['display_errors']) {
            $this->whoops->pushHandler(
                true === $this->container->get('request')->isXmlHttpRequest()
                    ? new JsonResponseHandler
                    : new PrettyPageHandler
            );
        } else {
            $this->whoops->pushHandler(function ($e, $inspector, $run) use ($self) {
                (new ConvertExceptionToResponse($self->whoops, $self->container, $self->logger))(
                    $self->container->get('request'),
                    $e
                );
            });
        }

        $this->whoops->register();
    }
}
