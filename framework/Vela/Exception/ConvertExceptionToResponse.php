<?php declare(strict_types = 1);

namespace Vela\Exception;

use Monolog\Logger;
use Vela\Container\Container;
use Vela\Request\Request;
use Vela\Response\Response;
use Vela\Routing\Exceptions\HttpException;
use Whoops\Run;

/**
 * Convert exception to response
 */
class ConvertExceptionToResponse
{
    /**
     * Container instance.
     *
     * @var \Vela\Container\Container
     */
    protected $container;

    /**
     * Construct
     *
     * @param \Whoops\Run       $whoops
     * @param \Vela\Container\Container $container
     * @param \Monolog\Logger    $logger
     */
    public function __construct(Run $whoops, Container $container, Logger $logger)
    {
        $this->whoops = $whoops;
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * Convert Exception to response.
     *
     * @param  \Vela\Request\Request  $request
     * @return \Vela\Response\Response
     */
    public function __invoke(Request $request, $e)
    {
        $code = ($e instanceof HttpException)
            ? $e->getCode()
            : 500;

        if (true === $this->container->get('config')->application['error_handler']['log_errors']) {
            $this->logger->error(($e->getMessage() ?: $e), ['exception' => $e]);
        }

        $response = new Response;

        $response->setContent(
            $this->container->view->view(
                sprintf("errors/%s.twig.php", $code)
            )
        );

        $response->setStatusCode(
            $code
        );

        return $response->send();
    }
}
