<?php declare(strict_types = 1);

namespace Vela\Database;

use Vela\Container\Container;

/**
 * Database class
 */
class Database
{
    /**
     * @var \Illuminate\Database\Capsule\Manager
     */
    protected $capsule;

    /**
     * Construct
     *
     * @param \Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->capsule = new \Illuminate\Database\Capsule\Manager;

        $this->capsule->addConnection(
            $container->config->database
        );

        $this->capsule->setAsGlobal();

        $this->capsule->bootEloquent();
    }

    /**
     * @return \Illuminate\Database\Capsule\Manager
     */
    public function getCapsule()
    {
        return $this->capsule;
    }
}
