<?php declare (strict_types = 1);

namespace Vela\Container;

use Psr\Container\ContainerInterface;
use Vela\Container\BaseContainer\BaseContainer;
use Vela\Container\Exception\NotFoundException;
use Vela\Container\ReflectionContainer;

/**
 * Container class
 */
class Container extends BaseContainer implements ContainerInterface
{
    /**
     * Create new definition and add to collection
     *
     * @param string  $id
     * @param mixed  $concrete
     * @param boolean $shared
     * @return \Vela\Container\Definition\Definition
     */
    public function add(string $id, $concrete = null, $shared = false)
    {
        return $this->definitions->add($id, $concrete, $shared);
    }

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        if ($this->definitions->has($id)) {
            return ($this->definitions->get($id))->resolve();
        }

        if ($this->reflectionContainer->has($id)) {
            return $this->reflectionContainer->get($id);
        }

        throw new NotFoundException;
    }

    /**
     * {@inheritdoc}
     */
    public function has($id): bool
    {
        if ($this->definitions->has($id)) {
            return true;
        }

        if ($this->reflectionContainer->has($id)) {
            return true;
        }

        return false;
    }

    /**
     * Add new shared definition
     *
     * @param  string $id
     * @param  mixed $concrete
     * @return \Vela\Container\Definition\Definition
     */
    public function share(string $id, $concrete = null)
    {
        return $this->add($id, $concrete, true);
    }

    /**
     * Create new definition without store in collection
     *
     * @param  string $id
     * @param  mixed $concrete
     * @param  bool $shared
     * @return \Vela\Container\Definition\Definition
     */
    public function shot(string $id, $concrete = null, $shared = false)
    {
        return $this->definitions->create($id, $concrete, $shared);
    }
}
