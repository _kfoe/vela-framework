<?php declare(strict_types = 1);

namespace Vela\Container\Traits;

use Vela\Container\Container;

/**
 * Trait used for retrive and set container
 */
trait ContainerAwareTrait
{
    /**
     * @var \Vela\Container\Container
     */
    protected $container;

    /**
     * Get the container.
     *
     * @return \Vela\Container\Container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set container
     *
     * @param \Vela\Container\Container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;

        return $this;
    }
}
