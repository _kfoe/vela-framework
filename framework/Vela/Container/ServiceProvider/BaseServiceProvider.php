<?php declare (strict_types = 1);

namespace Vela\Container\ServiceProvider;

use Vela\Container\ServiceProvider\ServiceProviderInterface;
use Vela\Container\Traits\ContainerAwareTrait;

/**
 * Base service provider
 */
abstract class BaseServiceProvider implements ServiceProviderInterface
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    abstract public function register();
}
