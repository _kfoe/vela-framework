<?php declare(strict_types = 1);

namespace Vela\Container\ServiceProvider;

use Vela\Container\Traits\ContainerAwareTrait;

/**
 * Service provider collection
 */
class ServiceProviderCollection
{
    use ContainerAwareTrait;

    /**
     * list of providers
     *
     * @var array
     */
    protected $providers = [];

    /**
     * Add service provider to collection
     *
     * @param string $provider
     */
    public function add(string $provider)
    {
        $provider = ($this->getContainer()->get($provider))
            ->setContainer($this->getContainer());

        $this->providers[] = $provider;
        
        $provider->register();
    }
}
