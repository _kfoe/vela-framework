<?php declare(strict_types = 1);

namespace Vela\Container\ServiceProvider;

interface ServiceProviderInterface
{
    /**
     * Use the register method to register items with the container
     *
     * @return void
     */
    public function register();
}
