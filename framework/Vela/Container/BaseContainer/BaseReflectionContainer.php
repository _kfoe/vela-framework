<?php declare (strict_types = 1);

namespace Vela\Container\BaseContainer;

use ReflectionMethod;
use ReflectionParameter;
use Vela\Container\Exception\ContainerException;

/**
 * BaseReflectionContainer class
 */
abstract class BaseReflectionContainer
{
    /**
     * Get construct dependencies
     *
     * @param  ReflectionMethod $construct
     * @return array
     */
    protected function getReflectorConstructDep(ReflectionMethod $construct)
    {
        return array_map(function (ReflectionParameter $dep) {
            return $this->resolveReflectedDep($dep);
        }, $construct->getParameters());
    }

    /**
     * called recursive for resolve dependencies
     *
     * @param  ReflectionParameter $dep
     * @return mixed
     */
    protected function resolveReflectedDep(ReflectionParameter $dep)
    {
        if (is_null($dep->getClass()->getName())) {
            throw new ContainerException(sprintf('class %s not found!', $dep->getClass()->getName()));
        }

        return $this->container->get(
            $dep->getClass()->getName()
        );
    }
}
