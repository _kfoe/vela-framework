<?php declare (strict_types = 1);

namespace Vela\Container\BaseContainer;

use Vela\Container\Definition\DefinitionCollection;
use Vela\Container\ReflectionContainer;
use Vela\Container\ServiceProvider\ServiceProviderCollection;

/**
 * BaseContainer class
 */
class BaseContainer
{
    /**
     * @var \Vela\Container\Definition\DefinitionCollection
     */
    protected $definitions;

    /**
     * @var \Vela\Container\ServiceProvider\ServiceProviderCollection
     */
    protected $providers;

    /**
     * @var \Vela\Container\ReflectionContainer
     */
    protected $reflectionContainer;

    /**
     * Construct
     */
    public function __construct()
    {
        $this->definitions = new DefinitionCollection;
        $this->providers = new ServiceProviderCollection;
        $this->reflectionContainer = new ReflectionContainer;

        $this->definitions->setContainer($this);
        $this->providers->setContainer($this);
        $this->reflectionContainer->setContainer($this);
    }

    /**
     * @param  string
     * @return mixed
     */
    public function __get(string $id)
    {
        return $this->get($id);
    }

    /**
     * Add providers to collection
     *
     * @param string $provider
     */
    public function addServiceProvider(string $provider)
    {
        $this->providers->add($provider);
    }

    /**
     * @return \Vela\Container\Definition\DefinitionCollection
     */
    public function getDefinitions()
    {
        return $this->definitions;
    }
}
