<?php declare (strict_types = 1);

namespace Vela\Container\Definition;

use ReflectionClass;
use Vela\Container\Traits\ContainerAwareTrait;

/**
 * Definition class
 */
class Definition
{
    use ContainerAwareTrait;

    /**
     * @var array
     */
    protected $arguments = [];

    /**
     * @var mixed
     */
    protected $concrete;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var array
     */
    protected $methods = [];

    /**
     * @var mixed
     */
    protected $resolved;

    /**
     * @var bool
     */
    protected $shared;

    /**
     * Construct
     *
     * @param string $id
     * @param mixed $concrete
     * @param bool $shared
     */
    public function __construct(string $id, $concrete, bool $shared)
    {
        $this->id = $id;
        $this->concrete = $concrete ?? $id;
        $this->shared = $shared;
    }

    public function getIsShared()
    {
        return $this->shared;
    }

    /**
     * @return mixed
     */
    public function resolve()
    {
        $concrete = $this->concrete;

        if ($this->shared && !is_null($this->resolved)) {
            return $this->resolved;
        }

        if (is_callable($concrete)) {
            $concrete = $this->resolveCallable(
                $concrete
            );
        }

        if (is_string($concrete) && class_exists($concrete)) {
            $concrete = $this->resolveClass(
                $concrete
            );
        }

        if (is_object($concrete)) {
            $concrete = $this->callMethods(
                $concrete
            );
        }

        $this->resolved = $concrete;

        return $concrete;
    }

    /**
     * Add argument to definition
     *
     * @param  mixed $arg
     * @return self
     */
    public function withArgument($arg)
    {
        $this->arguments[] = $arg;

        return $this;
    }

    /**
     * Add multiple arguments to definition
     *
     * @param  array  $args
     * @return self
     */
    public function withArguments(array $args)
    {
        foreach ($args as $arg) {
            $this->withArgument($arg);
        }

        return $this;
    }

    /**
     * Set method that will be call during resolve
     *
     * @param  string $method
     * @param  array  $args
     * @return self
     */
    public function withMethod(string $method, array $args = [])
    {
        $this->methods[] = array(
            'method' => $method,
            'args'   => $args,
        );

        return $this;
    }

    /**
     * Set methods that will be call during resolve
     *
     * @param  array $methods
     * @return self
     */
    public function withMethods(array $methods = [])
    {
        foreach ($methods as $method => $args) {
            $this->withMethod(
                $method,
                $args
            );
        }

        return $this;
    }

    /**
     * @return mixed
     */
    private function callMethods($concrete)
    {
        foreach ($this->methods as $method) {
            $concrete = call_user_func_array(
                array(
                    $concrete,
                    $method['method'],
                ),
                $method['args']
            );
        }

        return $concrete;
    }

    /**
     * @return mixed
     */
    private function resolveCallable($concrete)
    {
        return call_user_func_array(
            $concrete,
            $this->arguments
        );
    }

    /**
     * @return mixed
     */
    private function resolveClass($className)
    {
        return (new ReflectionClass($className))
            ->newInstanceArgs(
                $this->arguments
            );
    }
}
