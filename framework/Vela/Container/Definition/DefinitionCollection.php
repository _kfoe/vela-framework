<?php declare (strict_types = 1);

namespace Vela\Container\Definition;

use Vela\Container\Definition\Definition;
use Vela\Container\Exception\ContainerException;
use Vela\Container\Exception\NotFoundException;
use Vela\Container\Traits\ContainerAwareTrait;

/**
 * DefinitionCollection class
 */
class DefinitionCollection
{
    use ContainerAwareTrait;

    /**
     * @var \Vela\Container\Definition\Definition
     */
    protected $definitions = [];

    /**
     * Add new definition to collection
     *
     * @param string  $id
     * @param mixed  $concrete
     * @param boolean $shared
     * @throws Vela\Container\Exception\NotFoundException
     *
     * @return \Vela\Container\Definition\Definition
     */
    public function add(string $id, $concrete, bool $shared)
    {
        if ($this->has($id)) {
            throw new ContainerException('Identifier already exists!');
        }

        $definition = $this->create(
            $id,
            $concrete,
            $shared
        );

        $this->definitions[$id] = $definition;

        return $definition;
    }

    /**
     * Create new definition
     *
     * @param  string $id
     * @param  mixed $concrete
     * @param  bool   $shared
     * @return \Vela\Container\Definition\Definition
     */
    public function create(string $id, $concrete, bool $shared)
    {
        $definition = new Definition(
            $id,
            $concrete,
            $shared
        );

        $definition->setContainer(
            $this->container
        );

        return $definition;
    }

    /**
     * Retrive definition from collection
     *
     * @param  string $id
     * @throws Vela\Container\Exception\NotFoundException
     *
     * @return \Vela\Container\Definition\Definition
     */
    public function get(string $id)
    {
        if ($this->has($id)) {
            return $this->definitions[$id];
        }

        throw new NotFoundException;
    }

    /**
     * Check if definition exists
     *
     * @param  string  $id
     * @return boolean
     */
    public function has(string $id)
    {
        return null !== ($this->definitions[$id] ?? null);
    }
}
