<?php declare (strict_types = 1);

namespace Vela\Container;

use Psr\Container\ContainerInterface;
use ReflectionClass;
use Vela\Container\BaseContainer\BaseReflectionContainer;
use Vela\Container\Exception\ContainerException;
use Vela\Container\Traits\ContainerAwareTrait;

/**
 * ReflectionContainer class
 */
class ReflectionContainer extends BaseReflectionContainer implements ContainerInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function get($id)
    {
        $reflector = new ReflectionClass($id);

        if (!$reflector->isInstantiable()) {
            throw new ContainerException("Not instantiable!");
        }

        if ($construct = $reflector->getConstructor()) {
            return $reflector->newInstanceArgs(
                $this->getReflectorConstructDep($construct)
            );
        }

        return new $id();
    }

    /**
     * {@inheritdoc}
     */
    public function has($id)
    {
        return class_exists($id);
    }
}
