<?php

namespace Vela\Application\BaseMiddleware;

use Closure;
use Vela\Request\Request;

/**
 * Base middleware interface
 */
interface MiddlewareInterface
{
    /**
     * Run current layer then pass request to
     * next layer until core.
     *
     * @param  Vela\Request\Request $request
     * @param  Closure $next
     * @return mixed
     */
    public function peel(Request $request, Closure $next);
}
