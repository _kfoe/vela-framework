<?php

namespace Vela\Application\BaseMiddleware;

use Closure;
use Vela\Request\Request;

/**
 * BaseMiddleware class
 */
abstract class MiddlewareContract implements MiddlewareInterface
{
    /**
     * Run current layer then pass request to
     * next layer until core.
     *
     * @param  Vela\Request\Request $request
     * @param  Closure $next
     * @return mixed
     */
    abstract public function peel(Request $request, Closure $next);
}
