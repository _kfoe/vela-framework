<?php declare (strict_types = 1);

namespace Vela\Application\BaseApplication;

use App\Models\User;
use BCrypt;
use DB;
use ErrorException;
use Vela\Application\Requirements;
use Vela\Config\Config;

/**
 * Base Application.
 */
class BaseApplication
{
    /**
     * Container instance.
     *
     * @var Vela\Container\Container
     */
    public $container;

    /**
     * @var self
     */
    static $m = null;

    /**
     * return true if application has been started.
     *
     * @var boolean
     */
    protected $isApplicationStarted = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->prepareBoot();
    }

    /**
     * @return \Vela\Container\container
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Set container to facade
     *
     * @param string $original
     * @return void
     */
    public function setContainerToFacade(string $original)
    {
        $this->container->get($original)::setContainer($this->container);
    }

    /**
     * Boot service provider
     *
     * @param  array $serviceProvider
     * @return void
     */
    protected function loadServiceProvider(array $serviceProvider)
    {
        foreach ($serviceProvider as $s) {
            $this->container->addServiceProvider($s);
        }
    }

    /**
     * Boot requirements
     *
     * @return void
     */
    private function checkRequirements()
    {
        Requirements::check();
    }

    /**
     * Set class alias
     *
     * @param  string $original
     * @param  string $alias
     * @return void
     */
    private function classAlias(string $original, string $alias)
    {
        class_alias($original, $alias);
    }

    /**
     * Create Vela Container
     *
     * @return void
     */
    private function createContainer()
    {
        $this->container = new \Vela\Container\Container;
    }

    /**
     * Create demo database
     *
     * @return void
     */
    private function createDatabase()
    {
        $schema = DB::getCapsule()->schema();

        if (!$schema->hasTable('users')) {
            $schema->create('users', function ($table) {
                $table->increments('id');
                $table->string('username')->unique();
                $table->string('password');
                $table->string('session', 255)->nullable();
                $table->tinyInteger('is_online')->default(0);
                $table->timestamps();
            });

            User::create([
                'username' => 'demo',
                'password' => BCrypt::create('demo'),
            ]);
        }
    }

    /**
     * Convert Error to Exception and use
     * dieDebug function for print Exception.
     *
     * When application is loaded this handler
     * will be skipped for Whoops
     *
     * @return void
     */
    private function defaultExceptionHandler()
    {
        if (false === $this->isApplicationStarted) {
            error_reporting(E_ALL | E_STRICT);
            ini_set('display_errors', 'On');
            set_error_handler(function ($level, $message, $file, $line) {
                throw new ErrorException($message, 0, $level, $file, $line);
            });
            set_exception_handler(function ($e) {
                dd($e);
            });
        }
    }

    /**
     * Multibyte character encoding schemes and their related issues are fairly complicated,
     * and are beyond the scope of this documentation. Please refer to the following
     * @see https://secure.php.net/manual/en/ref.mbstring.php
     *
     * Timezone used by all date/time functions in a script
     *
     * @return void
     */
    private function defaultSettings()
    {
        # Used for encoding e-mail messages.
        mb_language('uni');

        # Set encoding for multibyte regex
        mb_regex_encoding(array_get($this->container->config->application, 'charset') ?? "UTF-8");

        # Set internal character encoding
        mb_internal_encoding(array_get($this->container->config->application, 'charset') ?? "UTF-8");

        # Timezone
        date_default_timezone_set(array_get($this->container->config->application, 'timezone') ?? "CET");
    }

    /**
     * Set class alias and container to facade
     *
     * @param  array  $aliases
     * @return void
     */
    private function loadAliases(array $aliases)
    {
        foreach ($aliases as $alias => $original) {
            $this->classAlias($original, $alias);
            $this->setContainerToFacade($original);
        }
    }

    /**
     * Prepare application for boot.
     *
     * @return void
     */
    private function prepareBoot()
    {
        $this->defaultExceptionHandler();

        $this->checkRequirements();

        $this->createContainer();

        $this->loadServiceProvider(
            array_get($this->container->get(Config::class)->application, 'providers')
        );

        $this->loadAliases(
            array_get($this->container->get(Config::class)->application, 'aliases')
        );

        $this->defaultSettings();

        $this->createDatabase();

        $this->isApplicationStarted = true;

        static::$m = $this;
    }
}
