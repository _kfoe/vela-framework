<?php declare(strict_types = 1);

namespace Vela\Application;

use RunTimeException;

/**
 * Requirements class
 */
class Requirements
{
    /**
     * Check system requirements before start.
     *
     * @return void
     */
    public static function check()
    {
        if (version_compare(PHP_VERSION, '7.1.3', '<')) {
            throw new RunTimeException(sprintf("VelaFramework requires php ^7.1.3. Your PHP version (%s) does not satisfy that requirement", PHP_VERSION));
        } elseif (!extension_loaded('json')) {
            throw new RunTimeException("VelaFramework requires JSON PHP Extension");
        } elseif (!extension_loaded('mbstring')) {
            throw new RunTimeException("VelaFramework requires Mbstring PHP Extension");
        } elseif (!extension_loaded('PDO') || !extension_loaded('pdo_mysql')) {
            throw new RunTimeException("VelaFramework requires PDO PHP Extension");
        } elseif (!extension_loaded('openssl')) {
            throw new RunTimeException("VelaFramework requires OpenSSL PHP Extension");
        }
    }
}
