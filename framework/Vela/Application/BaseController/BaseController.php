<?php declare (strict_types = 1);

namespace Vela\Application\BaseController;

use Vela\Container\Container;
use Vela\Request\Request;
use Vela\Routing\Route;


/**
 * Base Controller
 */
abstract class BaseController
{
    /**
     * Container instance
     *
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * Request instance
     *
     * @var Vela\Request\Request
     */
    protected $request;

    /**
     * Route instance
     *
     * @var Vela\Routing\Route
     */
    protected $route;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     * @param Vela\Request\Request   $request
     */
    public function __construct(Container $container, Request $request)
    {
        $this->container = $container;

        $this->request = $request;

        $this->route = $this->request->getRoute();
    }

    /**
     * Get param route if exists
     *
     * @param  string $name
     * @return mixed
     */
    public function __get(string $name)
    {
        return $this->route->getRouteParam()[$name] ?? null;
    }

    /**
     * Redirect to path or route
     *
     * @param  string $path
     * @return
     */
    public function redirect(string $r, array $param = [])
    {
        $route = str_replace('{?param:[a-z]+}', 'asd', $this->container->routeCollection->getRouteByName($r)->getUri());
        return redirect($route);
    }

    /**
     * Render view
     *
     * @param  string $view
     * @param  array  $param
     * @return \Vela\View\View
     */
    public function render(string $view, array $param = [])
    {
        return \View::view($view, $param);
    }
}
