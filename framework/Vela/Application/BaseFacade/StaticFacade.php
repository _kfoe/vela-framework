<?php

namespace Vela\Application\BaseFacade;

use Vela\Application\BaseFacade\BaseFacade;
use Vela\Application\BaseFacade\FacadeInterface;

/**
 * Static Facade
 */
abstract class StaticFacade extends BaseFacade implements FacadeInterface
{
    /**
     * Retrieves the Instance Identifier that is used to retrieve the context from the Container
     *
     * @return string
     */
    abstract public static function getInstanceIdentifier();
}
