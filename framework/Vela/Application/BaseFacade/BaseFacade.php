<?php declare(strict_types = 1);

namespace Vela\Application\BaseFacade;

use Vela\Application\Application;
use Vela\Container\Container;

/**
 * Base Facade
 */
abstract class BaseFacade
{
    /**
     * @var Vela\Container\Container
     */
    protected static $container;

    /**
     * @var resolved context
     */
    protected static $resolved = [];

    /**
     * Call instance
     *
     * @param  string $method
     * @param  array  $args
     * @return mixed
     */
    public static function __callStatic(string $method, array $args = [])
    {
        $instance = static::getFacadeInstance();

        if (count($args) > 4) {
            return call_user_func_array(
                [$instance, $method],
                $args
            );
        }

        return $instance->$method(...$args);
    }

    /**
     * Set container
     *
     * @param Vela\Container\Container $container
     */
    public static function setContainer(Container $container)
    {
        static::$container = $container;
    }

    /**
     * Get new or cached facade instance
     *
     * @return mixed
     */
    protected static function getFacadeInstance()
    {
        $accessor = static::getInstanceIdentifier();

        if (static::$container->getDefinitions()->get($accessor)->getIsShared() && isset(static::$resolved[$accessor])) {
            return static::$resolved[$accessor];
        }

        return static::$resolved[$accessor] = static::$container->get($accessor);
    }
}
