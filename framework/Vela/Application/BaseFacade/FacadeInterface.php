<?php

namespace Vela\Application\BaseFacade;

/**
 * Facade interface
 */
interface FacadeInterface
{
    /**
     * Retrieves the Instance Identifier that is used to retrieve the context from the Container
     *
     * @return string
     */
    public static function getInstanceIdentifier();
}
