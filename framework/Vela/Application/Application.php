<?php declare (strict_types = 1);

namespace Vela\Application;

use RunTimeException;
use Vela\Application\BaseApplication\BaseApplication;

/**
 * Application class
 */
class Application extends BaseApplication
{
    public static function getInstance()
    {
        if (!is_null(static::$m)) {
            return static::$m;
        }

        return new \Vela\Application\Application;
    }

    /**
     * Run application.
     *
     * @return Vela\Response\Response
     */
    public function run()
    {
        if (false === $this->isApplicationStarted) {
            throw new RunTimeException("Base application is not started.");
        }

        $response = $this->container->router->dispatch(
            $this->container->request
        );

        return $response;
    }
}
