<?php

namespace Vela\Request;

use Arr;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request as SymfonyRequest;
use Vela\Routing\Route;

/**
 * Request class
 */
class Request extends SymfonyRequest
{
    /**
     * @var \Vela\Routing\Route
     */
    protected $route;

    /**
     * Retrive and sanitize request param
     *
     * @param  string $a
     * @return mixed
     */
    public function __get(string $a)
    {
        $payload = $this->request->get($a);

        if (is_array($this->request->get($a))) {
            sanitizeArray($payload);
            return $payload;
        }

        return sanitize($payload) ?? null;
    }

    /**
     * Get all of the input and files for the request.
     *
     * @return array
     */
    public function all()
    {
        $r = $this->request->all();

        sanitizeArray($r);

        return $r;
    }

    /**
     * create a new HTTP request
     *
     * @return static
     */
    public static function capture()
    {
        static::enableHttpMethodParameterOverride();

        return static::createFromBase(SymfonyRequest::createFromGlobals());
    }

    /**
     * Create an request from a Symfony instance
     *
     * @param  \SymfonyRequest $request
     * @return \Vela\Request\Request
     */
    public static function createFromBase(SymfonyRequest $request)
    {
        if ($request instanceof static ) {
            return $request;
        }

        $content = $request->content;

        $request = (new static )->duplicate(
            $request->query->all(),
            $request->request->all(),
            $request->attributes->all(),
            $request->cookies->all(),
            $request->files->all(),
            $request->server->all()
        );

        $request->content = $content;
        $request->request = $request->getInputSource();

        return $request;
    }

    /**
     * Clones a request and overrides some of its parameters.
     *
     * @param array $query      The GET parameters
     * @param array $request    The POST parameters
     * @param array $attributes The request attributes (parameters parsed from the PATH_INFO, ...)
     * @param array $cookies    The COOKIE parameters
     * @param array $files      The FILES parameters
     * @param array $server     The SERVER parameters
     *
     * @return static
     */
    public function duplicate(array $query = null, array $request = null, array $attributes = null, array $cookies = null, array $files = null, array $server = null)
    {
        return parent::duplicate(
            $query,
            $request,
            $attributes,
            $cookies,
            $files,
            $server
        );
    }

    /**
     * @return \Vela\Routing\Route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Get a subset containing the provided keys with values from the input data.
     *
     * @param  array  $keys
     * @return array
     */
    public function only(array $keys)
    {
        return Arr::only($this->request->all(), $keys);
    }

    /**
     * Set route for current route
     *
     * @param \Vela\Routing\Route $route
     */
    public function setRoute(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Get the input source for the request
     *
     * @return \Symfony\Component\HttpFoundation\ParameterBag
     */
    protected function getInputSource()
    {
        if (0 === strpos($this->headers->get('Content-Type'), 'application/json')) {
            return new ParameterBag((array) json_decode($this->getContent(), true));
        }

        return in_array($this->getRealMethod(), ['GET', 'HEAD']) ? $this->query : $this->request;
    }
}
