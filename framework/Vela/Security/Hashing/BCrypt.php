<?php declare (strict_types = 1);

namespace Vela\Security\Hashing;

use Vela\Container\Container;

/**
 * BCrypt class
 */
class BCrypt
{
    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * Resolved hash
     *
     * @var array
     */
    protected $resolved;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Create a new hash
     *
     * @param  string  $plain
     * @param  integer $cost
     * @return string
     */
    public function create(string $plain)
    {
        $hash = password_hash(
            sha1($plain),
            PASSWORD_BCRYPT,
            array('cost' => array_get($this->container->config->security, 'BCRYPT_COST'))
        );

        if (!$hash) {
            throw new RuntimeException('Bcrypt not supported.');
        }

        return $hash;
    }

    /**
     * Check if need rehash
     *
     * @param  string $hash
     * @return boolean
     */
    public function needsRehash(string $hash)
    {
        return password_needs_rehash(
            $hash,
            PASSWORD_BCRYPT,
            array('cost' => array_get($this->container->config->security, 'BCRYPT_COST'))
        );
    }

    /**
     * Verify hash
     *
     * @param  string $plain
     * @param  string $hash
     * @return boolean
     */
    public function verify(string $plain, string $hash)
    {
        if (isset($this->resolved[$hash]) && $this->resolved[$hash] === $plain) {
            return true;
        }

        $resolved = password_verify(
            sha1($plain),
            $hash
        );

        if ($resolved) {
            $this->resolved[$hash] = $plain;
        }

        return $resolved;
    }
}
