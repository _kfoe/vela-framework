<?php

namespace Vela\Security\CSRF\Exception;

use Exception;
use Throwable;

class TokenMismatchException extends Exception implements Throwable
{
    //
}
