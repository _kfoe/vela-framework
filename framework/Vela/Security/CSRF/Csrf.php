<?php declare(strict_types = 1);

namespace Vela\Security\CSRF;

use BCrypt;
use Cookie;
use Session;
use Vela\Request\Request;

/**
 * Csrf class
 */
class Csrf
{
    /**
     * Store new token
     *
     * @return void
     */
    public function generate()
    {
        Cookie::set('_TOKEN', $this->token(), 1);
    }

    /**
     * Verify if provided token is valid
     *
     * @param  Vela\Request\Request $request
     * @return boolean
     */
    public function isValid(Request $request)
    {
        if ($this->shouldAddCsrfToken($request)) {
            return true;
        }

        $token = $request->cookies->get('_TOKEN');

        if ($token && Bcrypt::verify(Session::get('_TOKEN'), $token)) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the HTTP request uses a ‘read’ verb.
     *
     * @param  Vela\Request\Request  $request
     * @return bool
     */
    public function shouldAddCsrfToken($request)
    {
        return in_array($request->getMethod(), ['HEAD', 'GET', 'OPTIONS']);
    }

    /**
     * Generate a new token
     *
     * @return string
     */
    private function token()
    {
        $token = bin2hex(random_bytes(32));

        Session::set('_TOKEN', $token);

        return BCrypt::create($token);
    }
}
