<?php

namespace Vela\Security\CSRF\Middleware;

use Closure;
use Vela\Application\BaseMiddleware\MiddlewareContract;
use Vela\Request\Request;
use Vela\Security\CSRF\Csrf;
use Vela\Security\CSRF\Exception\TokenMismatchException;

/**
 * BaseMiddleware middlware.
 */
class BaseMiddleware extends MiddlewareContract
{
    /**
     * @var \Vela\Security\CSRF
     */
    protected $csrf;

    /**
     * Construct
     *
     * @param \Vela\Security\CSRF $csrf
     */
    public function __construct(Csrf $csrf)
    {
        $this->csrf = $csrf;
    }

    /**
     * {@inheritdoc}
     */
    public function peel(Request $request, Closure $next)
    {
        if (!in_array($request->getRoute()->getUri(), array_map(function ($v) {
            return trim($v, '/');
        }, $this->except)) && !$this->csrf->isValid($request)) {
            throw new TokenMismatchException;
        }

        if ($this->csrf->shouldAddCsrfToken($request)) {
            $this->csrf->generate();
        }

        return $next($request);
    }
}
