<?php declare (strict_types = 1);

namespace Vela\Cookie;

use Vela\Container\Container;

/**
 * Cookie class
 */
class Cookie
{
    /**
     * @var Vela\Config\Config;
     */
    private $config;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->config = $container->get('config');
    }

    /**
     * Clear a cookie with provided key
     *
     * @param  string $key
     * @return void
     */
    public function clear(string $key)
    {
        $this->set(
            $key,
            null,
            -2628000,
            array_get(
                $this->config->session,
                'cookie_options.path'
            ),
            array_get(
                $this->config->session,
                'cookie_options.domain'
            )
        );
    }

    /**
     * Check if cookie exists
     *
     * @param  string $key
     * @return boolean
     */
    public function exists(string $key)
    {
        return null !== ($_COOKIE[$key] ?? null);
    }

    /**
     * Create a forever cookie
     *
     * @param  string $key
     * @param  mixed $value
     * @return void
     */
    public function forever(string $key, $value)
    {
        $this->set($key, $value, 2147483647);
    }

    /**
     * Retrive cookie information or return default value
     *
     * @param  string $key
     * @param  mixed $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        if ($this->exists($key)) {
            return $_COOKIE[$key];
        }

        return $default;
    }

    /**
     * Set a cookie
     *
     * @param string  $key
     * @param mixed  $value
     * @param integer $minutes
     */
    public function set(string $key, $value, int $minutes = 1)
    {
        $expiry = time() + ($minutes * 60);

        setcookie(
            $key,
            $value,
            $expiry,
            array_get(
                $this->config->session,
                'cookie_options.path'
            ),
            array_get(
                $this->config->session,
                'cookie_options.domain'
            )
        );
    }
}
