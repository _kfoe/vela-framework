<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * View static facade
 */
class ViewFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "view";
    }
}
