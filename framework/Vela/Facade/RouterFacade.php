<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Router static facade
 */
class RouterFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "router";
    }
}
