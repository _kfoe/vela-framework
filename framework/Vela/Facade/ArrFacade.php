<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Arr static facade
 */
class ArrFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "arr";
    }
}
