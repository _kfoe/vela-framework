<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Database static facade
 */
class DBFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "database";
    }
}
