<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Auth static facade
 */
class AuthFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "auth";
    }
}
