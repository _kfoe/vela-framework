<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Bcrypt static facade
 */
class BCryptFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "bcrypt";
    }
}
