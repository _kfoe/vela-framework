<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Session static facade
 */
class SessionFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "session";
    }
}
