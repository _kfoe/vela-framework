<?php

namespace Vela\Facade;

use Vela\Application\BaseFacade\StaticFacade;

/**
 * Cookie static facade
 */
class CookieFacade extends StaticFacade
{
    public static function getInstanceIdentifier()
    {
        return "cookie";
    }
}
