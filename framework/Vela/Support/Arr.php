<?php declare (strict_types = 1);

namespace Vela\Support;

/**
 * Array class
 */
class Arr
{
    public function dropNumberKeys(array $array)
    {
        foreach ($array as $key => $value) {
            if (true === is_int($key)) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * Get first element in array
     *
     * @param  array $array
     * @return mixed
     */
    public function first(array $array)
    {
        $sliced_array = array_slice($array, 0, 1);

        return array_shift($sliced_array);
    }

    /**
     * Get an item from array using "dot" notation.
     * If item is an array it's possibile select
     * only key you need
     *
     * @param  array $array
     * @param  string $key
     * @param  array $only
     * @return mixed
     */
    public function get(array $array, string $key, array $only = [])
    {
        if (!is_array($array)) {
            return null;
        }

        if (is_null($key)) {
            return $array;
        }

        $parsed = explode('.', trim($key, '.'));

        while ($parsed) {
            $next = array_shift($parsed);
            if (isset($array[$next])) {
                $array = $array[$next];
            } else {
                return null;
            }
        }

        if (is_array($array) && count($only) > 0) {
            return $this->only($array, $only);
        }

        return $array;
    }

    /**
     * Determinate if the given value is array
     *
     * @param  mixed  $value
     * @return boolean
     */
    public function isArray($value)
    {
        return is_array($value);
    }

    /**
     * Get last element in array
     *
     * @param  array $array
     * @return mixed
     */
    public function last(array $array)
    {
        $sliced_array = array_slice(array_reverse($array, true), 0, 1);

        return array_shift($sliced_array);
    }

    /**
     * Return an array with only provided keys.
     *
     * @param  array $array
     * @param  array $keys
     * @return array
     */
    public function only(array $array, array $keys)
    {
        $exp = "/\b(" . join($keys, '|') . ")\b/";

        $keys = preg_grep($exp, array_keys($array), 0);

        $vals = [];

        foreach ($keys as $key) {
            $vals[$key] = $array[$key];
        }

        return $vals;
    }

    /**
     * Set item to array with dot notaiton
     *
     * @param array  &$array
     * @param string $key
     * @param mixed $value
     */
    public function set(array &$array, string $key, $value)
    {
        $parsed = explode('.', $key);

        $arr = &$array;

        if (count($array) <= 0) {
            $arr = &$_SESSION;
        }

        while (count($parsed) > 1) {
            $next = array_shift($parsed);

            if (!isset($arr[$next]) || !is_array($arr[$next])) {
                $arr[$next] = [];
            }

            $arr = &$arr[$next];
        }

        $arr[array_shift($parsed)] = $value;
    }
}
