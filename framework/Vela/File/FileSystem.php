<?php declare(strict_types = 1);

namespace Vela\File;

use FilesystemIterator;

/**
 * File system support
 */
class FileSystem
{
    /**
     * Return list file in directory
     *
     * @param  string $path
     * @return array
     */
    public function getFileInDirectory(string $path, array $matchOnly = [])
    {
        $found = [];

        foreach ((new FilesystemIterator($path)) as $file) {
            if ((count($matchOnly) > 0 && in_array($file->getExtension(), $matchOnly)) || count($matchOnly) <= 0 && !$file->isDir()) {
                $found[pathinfo($file->getFileName(), PATHINFO_FILENAME)] = $file->getPathName();
            }
        }

        return $found;
    }

    /**
     * Return true if a file exists.
     *
     * @param  string  $file
     * @return boolean
     */
    public function has(string $filePath)
    {
        return file_exists($filePath);
    }

    /**
     * Includes a file.
     *
     * @param  string  $file
     * @param  boolean $isOnce
     * @return mixed
     */
    public function includeFile(string $file, $isOnce = false)
    {
        if (true === $isOnce) {
            return include_once $file;
        }

        return include $file;
    }

    /**
     * Return true if the provided path is a directory.
     *
     * @param  string  $path
     * @return boolean
     */
    public function isDirectory(string $path)
    {
        return is_dir($path);
    }

    /**
     * Return true if the provided path is a file.
     *
     * @param  string  $path
     * @return boolean
     */
    public function isFile(string $filePath)
    {
        return is_file($filePath);
    }

    /**
     * Return information about filePath
     *
     * @param  string $filePath
     * @return array
     */
    public function pathInfo(string $filePath)
    {
        return pathinfo($filePath);
    }

    /**
     * Delete the file.
     *
     * @param  string $filePath
     * @return boolean
     */
    public function removeFile(string $filePath)
    {
        return unlink($filePath);
    }

    /**
     * Requires a file
     *
     * @param  string  $filePath
     * @param  boolean $isOnce
     * @return mixed
     */
    public function requireFile(string $filePath, $isOnce = false)
    {
        if (true === $isOnce) {
            return require_once $filePath;
        }

        return require $filePath;
    }
}
