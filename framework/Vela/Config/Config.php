<?php declare (strict_types = 1);

namespace Vela\Config;

use RunTimeException;
use Vela\Config\Loaders\Loader;

/**
 * Config class.
 */
class Config
{
    /**
     * Configuration
     *
     * @var array
     */
    protected $config = [];

    /**
     * Loader instance
     *
     * @var Vela\Config\Loaders\Loader
     */
    protected $loader = null;

    /**
     * Vela framework requires this lists.
     *
     * @var array
     */
    private $requireConfig = ['application', 'database', 'session', 'security'];

    /**
     * Constructor.
     *
     * @param \Vela\Config\Loaders\Loader $loader
     */
    public function __construct(Loader $loader)
    {
        $this->loader = $loader;

        $this->loadConfig();
    }

    /**
     * Get data from config file.
     *
     * @param  string $key
     * @return mixed
     */
    public function __get(string $key)
    {
        return $this->config[$key] ?? null;
    }

    /**
     * Load config file.
     *
     * @return void
     */
    public function loadConfig()
    {
        $fileLists = $this->loader->getFileSystem()->getFileInDirectory(
            basePath("app/config"),
            ['php']
        );

        if (count($fileLists) <= 0) {
            throw new RunTimeException("Missing config files!");
        }

        foreach ($fileLists as $fileName => $filePath) {
            if (in_array($fileName, $this->requireConfig)) {
                $this->config[$fileName] = $this->loader->load($filePath);
            }
        }
    }
}
