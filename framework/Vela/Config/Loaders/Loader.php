<?php declare(strict_types = 1);

namespace Vela\Config\Loaders;

use Exception;
use Vela\File\FileSystem;

/**
 * Loader class.
 */
class Loader
{
    /**
     * File system instance
     *
     * @var Vela\FileSystem\FileSystem
     */
    protected $fileSystem;

    /**
     * Constructor.
     *
     * @param Vela\File\FileSystem $fileSystem
     */
    public function __construct(FileSystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    /**
     * Return File system instance
     *
     * @return Vela\File\FileSystem
     */
    public function getFileSystem()
    {
        return $this->fileSystem;
    }

    /**
     * Loads the configuration file.
     *
     * @param  string $file
     * @return array
     */
    public function load(string $file)
    {
        if ($this->fileSystem->has($file)) {
            return $this->fileSystem->includeFile($file);
        }

        throw new Exception(sprintf('Cannot load file "%s" ', $file));
    }
}
