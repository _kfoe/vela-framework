<?php declare(strict_types = 1);

namespace Vela\ServiceProvider;

use Vela\Auth\Auth;
use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;

/**
 * Auth Service provider
 */
class AuthServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->share('auth', function () use ($container) {
            return new Auth($container);
        });
    }
}
