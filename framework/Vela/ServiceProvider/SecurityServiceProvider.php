<?php declare(strict_types = 1);

namespace Vela\ServiceProvider;

use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\Security\CSRF\Csrf;
use Vela\Security\Hashing\BCrypt;

/**
 * Security Service provider
 */
class SecurityServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->share('bcrypt', function () use ($container) {
            return new BCrypt($container);
        });
    }
}
