<?php declare (strict_types = 1);

namespace Vela\ServiceProvider;

use Dotenv\Dotenv;
use Vela\Config\Config;
use Vela\Config\Loaders\Loader;
use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\File\FileSystem;
use Vela\Request\Request;

/**
 * Application Service Provider
 */
class ApplicationServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->share('env', (Dotenv::create(basePath()))->load());

        $container->share('config', function () {
            return new Config(
                new Loader(
                    new FileSystem
                )
            );
        });

        $container->add('request', Request::capture());
    }
}
