<?php declare(strict_types = 1);

namespace Vela\ServiceProvider;

use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\Session\Session;

/**
 * Session Service Provider
 */
class SessionServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->add('session', function () use ($container) {
            return new Session($container);
        });

        $container->get('session');
    }
}
