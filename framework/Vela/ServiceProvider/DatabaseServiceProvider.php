<?php declare(strict_types = 1);

namespace Vela\ServiceProvider;

use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\Database\Database;

/**
 * Database Service Provider
 */
class DatabaseServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->add('database', function () use ($container) {
            return new Database($container);
        });

        $container->get("database");
    }
}
