<?php declare(strict_types = 1);

namespace Vela\ServiceProvider;

use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\Routing\RouteCollection;
use Vela\Routing\RouteFactory;
use Vela\Routing\Router;

/**
 * Routing Service Provider
 */
class RoutingServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->add('router', function () use ($container) {
            $router = new Router($container);
            require_once basePath('app/routes.php');
            return $router;
        });

        $container->share('routeCollection', (new RouteCollection($container)));

        $container->share('routeFactory', (new RouteFactory($container)));
    }
}
