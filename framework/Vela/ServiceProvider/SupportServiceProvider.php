<?php declare(strict_types = 1);

namespace Vela\ServiceProvider;

use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\Support\Arr;

/**
 * Support Service provider
 */
class SupportServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->share('arr', Arr::class);
    }
}
