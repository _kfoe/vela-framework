<?php declare (strict_types = 1);

namespace Vela\ServiceProvider;

use Twig_Environment;
use Twig_Loader_Filesystem;
use Vela\Container\ServiceProvider\BaseServiceProvider;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\View\View;

/**
 * View Service Provider
 */
class ViewServiceProvider extends BaseServiceProvider
{
    use ContainerAwareTrait;

    /**
     * Use the register method to set definitios to container
     *
     * @return void
     */
    public function register()
    {
        $container = $this->getContainer();

        $container->add('view', function () use ($container) {
            return new View(
                $container,
                new Twig_Environment(
                    new Twig_Loader_Filesystem(basePath("app/Views"))
                )
            );
        });
    }
}
