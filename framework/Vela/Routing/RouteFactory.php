<?php declare (strict_types = 1);

namespace Vela\Routing;

use Vela\Container\Container;
use Vela\Routing\Route;

/**
 * RouteFactory class
 */
class RouteFactory
{
    /**
     * @var array
     */
    public $middleware = [];

    /**
     * @var array
     */
    protected $builded = [];

    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * @var string
     */
    protected $prefix;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Add middleware to group
     */
    public function addMiddleware()
    {
        foreach ($this->builded as $m) {
            $m->middleware(
                $this->middleware
            );
        }
    }

    /**
     * Build a Route instance
     *
     * @param  array  $methods
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function build(array $methods, string $uri, string $controllerAction)
    {
        $uri = $this->prefix . $uri;

        $route = $this->builded[] = new Route($methods, $uri, $controllerAction);

        $route->setContainer($this->container);

        return $route;
    }

    /**
     * @return string
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param string $prefix
     *
     * @return Vela\Routing\RouteFactory
     */
    public function setPrefix(string $prefix)
    {
        $this->prefix = trim($prefix, '/') . '/';
        
        return $this;
    }
}
