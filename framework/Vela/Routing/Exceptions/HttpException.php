<?php

namespace Vela\Routing\Exceptions;

use Exception;
use Throwable;

/**
 * HttpException exception
 */
class HttpException extends Exception implements Throwable
{
}
