<?php

namespace Vela\Routing\Exceptions;

use Exception;
use Throwable;

/**
 * BadRouteException exception
 */
class BadRouteException extends Exception implements Throwable
{

}
