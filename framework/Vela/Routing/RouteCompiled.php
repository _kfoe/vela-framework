<?php declare (strict_types = 1);

namespace Vela\Routing;

use Vela\Routing\Route;

/**
 * Route compiled class
 */
class RouteCompiled
{
    const REGEX_FOR_MATCH = "`(?<=^|\})[^\][\'\s\x80-\xFF]+?(?=\{|$)|(\{(\?)?([a-z]+):([^\}]+)\}+)`";

    /**
     * regex for match static part of route
     */
    const STATIC_PART_REGEX = "`(?<=^|\})[^\]\[\?]+?(?=\{|$)`";

    /**
     * Route regex
     *
     * @var string
     */
    private $regex;

    /**
     * Route instance
     *
     * @var Vela\Routing\Route
     */
    private $route;

    /**
     * Constructor
     *
     * @param Vela\Routing\Route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;

        $this->compile();
    }

    /**
     * Compile regex route
     *
     * @return void
     */
    private function compile()
    {
        $this->buildRegex();
    }

    /**
     * @return string
     */
    public function getRegex()
    {
        return $this->regex;
    }

    /**
     * @return Vela\Routing\Route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Generate regex with optional param
     * and optional slash.
     *
     * @param  string  $paramName
     * @param  string  $paramRegex
     * @param  boolean $isOptionalSlash
     * @return string
     */
    private function optionalParam($paramName, $paramRegex, $isOptionalSlash = false)
    {
        return sprintf((true === $isOptionalSlash)
                ? "(?:/(?P<%s>%s))?"
                : "(?P<%s>%s)?", $paramName, $paramRegex);
    }

    /**
     * Generate regex with required param
     *
     * @param  string  $paramName
     * @param  string  $paramRegex
     * @return string
     */
    private function requiredParam($paramName, $paramRegex)
    {
        return sprintf("(?P<%s>%s)", $paramName, $paramRegex);
    }

    /**
     * Build regexp
     *
     * @return string
     */
    private function buildRegex()
    {
        $regex = "";

        $uri = $this->route->getUri();

        preg_match_all(static::REGEX_FOR_MATCH, $uri, $matches);

        $isOptionalSlash = false;

        foreach ($matches[0] as $key => $value) {
            if (empty($matches[1][$key])) {
                if (substr($value, -1, 1) === "/" && ($matches[2][($key + 1)] ?? null) === "?") {
                    $regex .= rtrim($value, "/");
                    $isOptionalSlash = true;
                } else {
                    $regex .= $value;
                }
            } else {
                if ("?" === $matches[2][$key]) {
                    $regex .= $this->optionalParam($matches[3][$key], $matches[4][$key], $isOptionalSlash);
                } else {
                    $regex .= $this->requiredParam($matches[3][$key], $matches[4][$key]);
                }
            }
        }

        $this->regex = sprintf('#^%s$#sDu', empty($regex) ? $uri : $regex);
    }
}
