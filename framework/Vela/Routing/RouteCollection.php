<?php declare (strict_types = 1);

namespace Vela\Routing;

use Vela\Container\Container;
use Vela\Request\Request;
use Vela\Routing\Route;
use Vela\Routing\RouteCompiled;
use Vela\Routing\RouteMatch;

/**
 * RouteCollection class
 */
class RouteCollection
{
    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * @var array
     */
    protected $routeName = [];

    /**
     * @var array
     */
    protected $routes = [];

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Add route to collection
     *
     * @param Vela\Routing\Route $route
     * @return Vela\Routing\Route
     */
    public function add(Route $route)
    {
        $this->addToCollection($route);

        return $route;
    }

    /**
     * add route by name
     *
     * @param  Vela\Routing\Route  $route
     * @return void
     */
    public function addRouteByName(Route $route)
    {
        foreach ($route->getMethods() as $method) {
            $this->routeName[$route->getName()][$method] = $route;
        }
    }

    /**
     * Compile routes
     *
     * @param  Vela\Request\Request $request
     * @return Vela\Routing\RouteCompiled
     */
    public function compile(Request $request)
    {
        foreach ($this->routes as $route) {
            foreach ($route as $r) {
                $r->setCompiled(
                    $this->compileRoute($request, $r)
                );
            }
        }
    }

    /**
     * Get a route that responds to provided name and|or http method
     *
     * @param  string $method
     * @param  string $name
     * @return Vela\Routing\Route|null
     */
    public function getRouteByName(string $name, string $method = null)
    {
        return $this->routeName[$name][(null !== $method ? strtoupper($method) : $method)] ?? array_values(($this->routeName[$name] ?? []))[0] ?? null;
    }

    /**
     * Get all registered routes
     *
     * @return array
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Get route by uri and http method
     *
     * @param  string  $method
     * @param  string  $uri
     * @return boolean
     */
    public function hasByMethod(string $method, string $uri)
    {
        return $this->routes[$method][$uri] ?? null;
    }

    /**
     * Check if request uri match with an existing route
     *
     * @param  Vela\Request\Request $request
     * @return int
     */
    public function match(Request $request)
    {
        return (new RouteMatch($this->container))->through($request);
    }

    /**
     * Add route to collection
     *
     * @param Vela\Routing\Route $route
     */
    private function addToCollection(Route $route)
    {
        foreach ($route->getMethods() as $method) {
            $this->routes[$method][$route->getUri()] = $route;
        }
    }

    /**
     * Compile a route
     *
     * @param  Vela\Request\Request $request
     * @param  Vela\Routing\Route   $route
     * @return Vela\Routing\RouteCompiled
     */
    private function compileRoute(Request $request, Route $route)
    {
        return (new RouteCompiled($route));
    }
}
