<?php declare (strict_types = 1);

namespace Vela\Routing;

use Arr;
use Vela\Container\Traits\ContainerAwareTrait;
use Vela\Routing\Route;
use Vela\Routing\RouteCompiled;

/**
 * Route class
 */
class Route
{
    use ContainerAwareTrait;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var Vela\Routing\RouteCompiled
     */
    protected $compiled;

    /**
     * @var string
     */
    protected $controller;

    /**
     * @var string
     */
    protected $controllerAction;

    /**
     * @var string
     */
    protected $controllerNamespace = "App\\Http\\Controllers\\";

    /**
     * @var array
     */
    protected $methods = [];

    /**
     * @var array
     */
    protected $middleware = [];

    /**
     * @var string
     */
    protected $middlewareNamespace = "App\\Http\\middleware\\";

    /**
     * @var string
     */
    protected $name;

    /**
     * @var array
     */
    protected $routeParam = [];

    /**
     * @var string
     */
    protected $uri;

    /**
     * Construct
     *
     * @param array  $methods
     * @param string $uri
     * @param string $controllerAction
     * @return void
     */
    public function __construct(array $methods, string $uri, string $controllerAction)
    {
        $this->methods = $methods;

        $this->uri = $this->prepareUri(
            $uri
        );

        $this->parseControllerAction(
            $controllerAction
        );
    }

    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return Vela\Routing\RouteCompiled
     */
    public function getCompiled()
    {
        return $this->compiled;
    }

    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getControllerAction()
    {
        return $this->controllerAction;
    }

    /**
     * @return mixed
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRouteParam()
    {
        return $this->routeParam;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getmiddleware()
    {
        return $this->prepareMiddleware(
            $this->middleware
        );
    }

    /**
     * Alias function
     *
     * @param  array  $middleware
     * @return Vela\Routing\Route
     */
    public function middleware(array $middleware)
    {
        $this->setmiddleware($middleware);

        return $this;
    }

    /**
     * Alias function
     *
     * @param  array  $middleware
     * @return Vela\Routing\Route
     */
    public function name(string $name)
    {
        $this->setName($name);

        return $this;
    }

    /**
     * @param Vela\Routing\RouteCompiled $compiled
     *
     * @return Vela\Routing\Route
     */
    public function setCompiled(RouteCompiled $compiled)
    {
        $this->compiled = $compiled;

        return $this;
    }

    /**
     * @param mixed $controllerAction
     *
     * @return Vela\Routing\Route
     */
    public function setControllerAction($controllerAction)
    {
        $this->controllerAction = $controllerAction;

        return $this;
    }

    /**
     * @param mixed $methods
     *
     * @return Vela\Routing\Route
     */
    public function setMethods($methods)
    {
        $this->methods = $methods;

        return $this;
    }

    /**
     * @param mixed $name
     *
     * @return Vela\Routing\Route
     */
    public function setName($name)
    {
        $this->name = $name;

        $this->getContainer()->routeCollection->addRouteByName(
            $this
        );

        return $this;
    }

    /**
     * @param mixed $routeParam
     *
     * @return Vela\Routing\Route
     */
    public function setRouteParam($routeParam)
    {
        $routeParam = array_drop_number_keys($routeParam);

        foreach ($routeParam as $n => $v) {
            $this->routeParam[$n] = $v[0];

            if (env('FORCE_NUMERIC_CAST') && is_numeric($v[0])) {
                $this->routeParam[$n] = (strpos($v[0], '.') !== false) ? (float) $v[0] : (int) $v[0];
            }
        }

        return $this;
    }

    /**
     * @param mixed $uri
     *
     * @return Vela\Routing\Route
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @param mixed $middleware
     *
     * @return Vela\Routing\Route
     */
    public function setmiddleware($middleware)
    {
        $this->middleware = $middleware;

        return $this;
    }

    /**
     * Set controller and action
     *
     * @param  string $controllerAction
     * @return void
     */
    private function parseControllerAction(string $controllerAction)
    {
        list($controller, $action) = explode('@', $controllerAction);

        $this->controller = $this->controllerNamespace . $controller;

        $this->action = $action;

        $this->controllerAction = $controllerAction;
    }

    private function prepareMiddleware(array $middleware)
    {
        foreach ($middleware as $k => $c) {
            $middleware[$k] = $this->middlewareNamespace . $c;
        }

        return $middleware;
    }

    /**
     * Clean up uri
     *
     * @param  string $uri
     * @return string
     */
    private function prepareUri(string $uri)
    {
        $uri = str_replace('\\', "/", $uri);

        $uri = preg_replace('/\s+/', '', $uri);

        $uri = preg_replace('/(\/)+/', '/', $uri);

        return trim($uri, '/');
    }
}
