<?php declare (strict_types = 1);

namespace Vela\Routing;

use Exception;
use Vela\Container\Container;
use Vela\Request\Request;

/**
 * Pipeline class
 */
class Pipeline
{
    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * Controller called after|before middleware
     *
     * @var Vela\Application\BaseController\BaseController
     */
    protected $end;

    /**
     * Layers before and after end function
     *
     * @var array
     */
    protected $layers = [];

    /**
     * @var Vela\Request\Request
     */
    protected $request;

    /**
     * @var Vela\Routing\Route
     */
    protected $route;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     * @param Vela\Request\Request   $request
     */
    public function __construct(Container $container, Request $request)
    {
        $this->container = $container;

        $this->request = $request;

        $this->route = $request->getRoute();
    }

    /**
     * Prepare for run pipeline logic
     *
     * @return mixed
     */
    public function prepare()
    {
        $this->prepareMiddleware(
            array_merge(
                array_get($this->container->config->application, 'middleware'),
                $this->route->getMiddleware()
            )
        );

        $this->prepareCoreFunction(
            $this->route->getController(),
            $this->route->getAction()
        );

        return $this->run();
    }

    /**
     * Prepare core function that will provide to
     * run controller and action for current route.
     *
     * @param  string $controller
     * @param  string $action
     * @return Closure
     */
    private function prepareCoreFunction(string $controller, string $action)
    {
        $container = $this->container;

        $route = $this->route;

        $this->end = function ($request) use ($container, $route, $controller, $action) {
            try {
                return $this->container->shot($controller)
                    ->withArguments([
                        $container,
                        $request,
                    ])
                    ->withMethod(
                        $action,
                        $route->getRouteParam()
                    )
                    ->resolve()
                ;
            } catch (Exception $e) {
                throw $e;
            }
        };
    }

    /**
     * Add middleware to layers.
     *
     * @param  array  $middleware
     * @return void
     */
    private function prepareMiddleware(array $middleware)
    {
        $this->layers = array_merge(
            $this->layers,
            array_reverse($middleware)
        );
    }

    /**
     * Run layers around core function and pass the
     * current request through it
     *
     * @return mixed
     */
    private function run()
    {
        $complete = array_reduce($this->layers, function ($nextLayer, $layer) {
            return function ($request) use ($nextLayer, $layer) {
                try {
                    #TODO: add is instanceOf BaseMiddleware.
                    return ($this->container->get($layer))
                        ->peel($request, $nextLayer)
                    ;
                } catch (Exception $e) {
                    throw $e;
                }
            };
        }, $this->end);

        return $complete($this->request);
    }
}
