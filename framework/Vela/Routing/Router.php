<?php declare (strict_types = 1);

namespace Vela\Routing;

use Closure;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Vela\Container\Container;
use Vela\Request\Request;
use Vela\Response\JsonResponse;
use Vela\Response\Response;
use Vela\Routing\Exceptions\HttpException;
use Vela\Routing\Route;
use Vela\Routing\RouteCollection;
use Vela\Routing\RouteFactory;

/**
 * Router class
 */
class Router
{
    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * @var Vela\Routing\RouteFactory
     */
    protected $routeFactory;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Register a new DELETE route with the router.
     *
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function delete(string $uri, string $controllerAction)
    {
        return $this->add(['DELETE'], $uri, $controllerAction);
    }

    /**
     * Dispatcher
     *
     * @param  Vela\Request\Request  $request
     * @return Vela\Response\Response
     */
    public function dispatch(Request $request)
    {
        $match = $this->container->routeCollection->match(
            $request
        );

        return $this->prepareResponse(
            $request,
            $match
        );
    }

    /**
     * Register a new GET route with the router.
     *
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function get(string $uri, string $controllerAction)
    {
        return $this->add(['GET', 'HEAD'], $uri, $controllerAction);
    }

    /**
     * Create a route group with a common prefix
     *
     * @param  string  $prefix
     * @param  Closure $closure
     * @return Vela\Routing\Router
     */
    public function group(string $prefix, Closure $closure)
    {
        $before = $this->container->routeFactory->getPrefix();

        $prefix = $before . $prefix;

        $this->container->routeFactory->setPrefix($prefix);

        $closure();

        $prefix = $before ?? "";

        $this->container->routeFactory->setPrefix($prefix);

        return $this;
    }

    /**
     * Add middleware to group
     *
     * @param  array  $middleware
     * @return void
     */
    public function middleware(array $middleware)
    {
        $this->container->routeFactory->middleware = $middleware;

        $this->container->routeFactory->addMiddleware();
    }

    /**
     * Register a new OPTIONS route with the router.
     *
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function options(string $uri, string $controllerAction)
    {
        return $this->add(['OPTIONS'], $uri, $controllerAction);
    }

    /**
     * Register a new PATCH route with the router.
     *
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function patch(string $uri, string $controllerAction)
    {
        return $this->add(['PATCH'], $uri, $controllerAction);
    }

    /**
     * Register a new POST route with the router.
     *
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function post(string $uri, string $controllerAction)
    {
        return $this->add(['POST'], $uri, $controllerAction);
    }

    /**
     * Register a new PUT route with the router.
     *
     * @param  string $uri
     * @param  string $controllerAction
     * @return Vela\Routing\Route
     */
    public function put(string $uri, string $controllerAction)
    {
        return $this->add(['PUT'], $uri, $controllerAction);
    }

    /**
     * Add new route to collection
     *
     * @param array  $methods
     * @param string $uri
     * @param string $controllerAction
     * @return Vela\Routing\Route
     */
    private function add(array $methods, string $uri, string $controllerAction)
    {
        return $this->container->routeCollection->add(
            $this->container->routeFactory->build(
                $methods,
                $uri,
                $controllerAction
            )
        );
    }

    private function callPipeline(Request $request)
    {
        return (new Pipeline($this->container, $request))->prepare();
    }

    /**
     * Create a response instance from the given value.
     *
     * @param  Vela\Request\Request $request
     * @param  mixed  $response
     * @return Vela\Response\Response|Vela\Response\JsonResponse
     */
    private function end(Request $request, $response)
    {
        if (!$response instanceof SymfonyResponse &&
            ($response instanceof ArrayObject ||
                $response instanceof JsonSerializable ||
                is_array($response))) {
            $response = new JsonResponse($response);
        } elseif (!$response instanceof SymfonyResponse) {
            $response = new Response($response);
        }

        if ($response->getStatusCode() === 304) {
            $response->setNotModified();
        }

        return $response
            ->prepare($request)
            ->send();
    }

    private function prepareResponse(Request $request, int $match)
    {
        switch ($match) {
            case 0:
                throw new HttpException('', 404);
                break;
            case 1:
                throw new HttpException('', 405);
                break;
            case 2:
                $this->end(
                    $request,
                    $this->callPipeline(
                        $request
                    )
                );
                break;
            default:
                throw new HttpException('', 500);
                break;
        }
    }
}
