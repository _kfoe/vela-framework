<?php

namespace Vela\Routing;

use Vela\Container\Container;
use Vela\Request\Request;
use Vela\Routing\Route;
use Vela\Routing\RouteCollection;

/**
 * RouteMatch class
 */
class RouteMatch
{
    /**
     * Route found but method not allowed
     */
    const METHOD_NOT_ALLOW = 1;

    /**
     * Route found
     */
    const ROUTE_FOUND = 2;

    /**
     * Route not found
     */
    const ROUTE_NOT_FOUND = 0;

    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Determinate if request uri match against route
     *
     * @param  Vela\Request\Request $request
     * @return array
     */
    public function through(Request $request)
    {
        $this->container->routeCollection->compile(
            $request
        );

        foreach ($this->container->routeCollection->getRoutes() as $route) {
            foreach ($route as $r) {
                $match = preg_match_all($r->getCompiled()->getRegex(), trim($request->getRequestUri(), '/'), $param);

                if ($match) {
                    $currentMatchedRoute = $this->container->routeCollection->hasByMethod(
                        $request->getMethod(),
                        $r->getUri()
                    );

                    if (null == $currentMatchedRoute) {
                        return self::ROUTE_NOT_FOUND;
                    }

                    if (!in_array($request->getMethod(), $currentMatchedRoute->getMethods())) {
                        return self::METHOD_NOT_ALLOW;
                    }

                    $request->setRoute(
                        $currentMatchedRoute
                    );

                    $currentMatchedRoute->setRouteParam(
                        $param
                    );

                    return self::ROUTE_FOUND;
                }
            }
        }
        return self::ROUTE_NOT_FOUND;
    }
}
