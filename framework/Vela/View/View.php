<?php declare (strict_types = 1);

namespace Vela\View;

use Vela\Container\Container;
use Vela\View\BaseView\BaseView;

/**
 * View class
 */
class View extends BaseView
{
    /**
     * Render a template with optional param
     *
     * @param  string $view
     * @param  array  $data
     * @return view
     */
    public function view(string $view, array $data = [])
    {
        return $this->twig->render(
            $view,
            $data
        );
    }
}
