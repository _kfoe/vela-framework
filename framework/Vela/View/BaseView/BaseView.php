<?php declare (strict_types = 1);

namespace Vela\View\BaseView;

use Twig_Environment;
use Vela\Container\Container;

/**
 * BaseView class
 */
class BaseView
{
    /**
     * @var Vela\Container\Container;
     */
    protected $container;

    /**
     * @var Twig_Environment
     */
    protected $twig;

    /**
     * Custom php functions that will be used in twig template
     *
     * @var array
     */
    protected $twigFunctions = array(
        'ddump',
        'auth',
        'assets',
        'route',
        'method_field',
    );

    /**
     * Construct
     *
     * @param Vela\Container\Container        $container
     * @param Twig_Environment $twig
     */
    public function __construct(Container $container, Twig_Environment $twig)
    {
        $this->container = $container;

        $this->twig = $twig;

        $this->loadFunctions();
    }

    /**
     * Load custom php functions before render twig template
     *
     * @return void
     */
    private function loadFunctions()
    {
        foreach ($this->twigFunctions as $f) {
            $this->twig->addFunction(
                call_user_func($f)
            );
        }
    }
}
