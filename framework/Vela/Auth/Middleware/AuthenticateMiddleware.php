<?php

namespace Vela\Auth\Middleware;

use Auth;
use Closure;
use Vela\Application\BaseMiddleware\MiddlewareContract;
use Vela\Request\Request;

/**
 * Authenticate middlware.
 */
class AuthenticateMiddleware extends MiddlewareContract
{
    /**
     * {@inheritdoc}
     */
    public function peel(Request $request, Closure $next)
    {
        if (Auth::check()) {
            Auth::setUserFromSession();
        }

        return $next($request);
    }
}
