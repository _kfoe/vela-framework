<?php declare (strict_types = 1);

namespace Vela\Auth;

use App\Models\User;
use BCrypt;
use Cookie;
use Session;
use Vela\Container\Container;

/**
 * Auth class
 */
class Auth
{
    /**
     * @var App\Models\User
     */
    public $user;

    /**
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Check if credentials are valid
     *
     * @param  string $username
     * @param  string $password
     * @return boolean
     */
    public function attempt(string $username, string $password)
    {
        $user = $this->getUserByUsername($username);

        if (!$user || !$this->hasValidCredentials($user, $password)) {
            return false;
        }

        if (BCrypt::needsRehash($user->password)) {
            $this->updateUserPasswordHash($user, $password);
        }

        $this->setSecretServer();

        $this->setUserSession($user);

        $this->setSessionUserData($user);

        $this->setUser($user);

        return true;
    }

    /**
     * Check if user is logged
     *
     * @return boolean
     */
    public function check()
    {
        return null !== Session::get('user.secret') && BCrypt::verify(
            array_get($this->container->config->security, 'SECRET'),
            Session::get('user.secret')
        );
    }

    /**
     * Logout user
     *
     * @return void
     */
    public function logout()
    {
        Session::close();
    }

    /**
     * Set user from session
     */
    public function setUserFromSession()
    {
        if ($user = User::where("id", Session::get('user.data.id'))->first()) {

            $user->update([
                'is_online' => 1,
            ]);

            $this->user = $user;
        }
    }

    /**
     * Retrive model about user logged
     *
     * @return App\Models\Users
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Retrive model about user from username
     *
     * @param  string $username
     * @return null|App\Models\Users
     */
    private function getUserByUsername(string $username)
    {
        return User::select([
            'id',
            'username',
            'password',
        ])->where('username', $username)->first() ?? null;
    }

    /**
     * Verify if stored hash match with provided password
     *
     * @param  App\Models\User    $user
     * @param  string  $password
     * @return boolean
     */
    private function hasValidCredentials(User $user, string $password)
    {
        return BCrypt::verify($password, $user->password);
    }

    /**
     * Set session with secret that authorize user
     */
    private function setSecretServer()
    {
        Session::set('user.secret', BCrypt::create(
            array_get($this->container->config->security, 'SECRET')
        ));
    }

    private function setSessionUserData($user)
    {
        Session::set('user.data', $user->toArray());
    }

    /**
     * Set current user
     *
     * @param App\Models\User $user
     */
    private function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Update hash session in database for current user
     *
     * @param App\Models\User $user
     */
    private function setUserSession(User $user)
    {
        $user->update([
            'session' => BCrypt::create(
                Cookie::get(array_get($this->container->config->session, 'session_name'))
            ),
        ]);
    }

    /**
     * Update stored hash if needed
     *
     * @param  App\Models\User   $user
     * @param  string $password
     */
    private function updateUserPasswordHash(User $user, string $password)
    {
        $user->update([
            "password" => BCrypt::create($password),
        ]);
    }
}
