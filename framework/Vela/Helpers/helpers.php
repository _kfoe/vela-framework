<?php

if (!function_exists("app")) {
    /**
     * Get an instance of application
     *
     * @return \Vela\Application\Application
     */
    function app()
    {
        return \Vela\Application\Application::getInstance();
    }
}

if (!function_exists('basePath')) {
    /**
     * Get base path of application
     *
     * @param  string $path
     * @return string
     */
    function basePath($path = '')
    {
        return __DIR__ . "/../../../" . trim($path, '/');
    }
}

if (!function_exists('isHttps')) {
    /**
     * Verify if server serve ssl
     *
     * @return boolean
     */
    function isHttps()
    {
        $cf_key = ['HTTP_CF_VISITOR', 'HTTP_X_FORWARDED_PROTO'];
        foreach ($cf_key as $key) {
            if (isset($_SERVER[$key]) && (strpos($_SERVER[$key], 'https') !== false)) {
                $_SERVER['HTTPS'] = 'on';
            }
        }
        if (isset($_SERVER['HTTPS'])) {
            if (1 === $_SERVER['HTTPS']) {
                return true;
            } elseif ('on' === $_SERVER['HTTPS']) {
                return true;
            }
        } elseif (443 === $_SERVER['SERVER_PORT']) {
            return true;
        }
        return false;
    }
}

if (!function_exists('currentUrl')) {
    /**
     * get current url
     *
     * @return string
     */
    function currentUrl()
    {
        $host = $_SERVER['HTTP_HOST'];

        $proto = "https://";

        $uri = $_SERVER['REQUEST_URI'];

        if (!isHttps()) {
            $proto = "http://";
        }

        return rtrim($proto . $host . $uri, "/") . "/";
    }
}

if (!function_exists('baseUrl')) {
    /**
     * Get base url
     *
     * @return string
     */
    function baseUrl()
    {
        $host = $_SERVER['HTTP_HOST'];

        $proto = "https://";

        if (!isHttps()) {
            $proto = "http://";
        }

        return $proto . $host . "/";
    }
}

if (!function_exists("ip")) {
    /**
     * get ip
     *
     * @return string
     */
    function ip()
    {
        $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');

        foreach ($ip_keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip);

                    if (validateIp($ip)) {
                        return $ip;
                    }
                }
            }
        }

        return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : -1;
    }
}

if (!function_exists("validateIp")) {
    /**
     * validate ip
     *
     * @param  string $ip
     * @return string
     */
    function validateIp(string $ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_IPV6 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
            return false;
        }

        return true;
    }
}

if (!function_exists("redirect")) {
    /**
     * Redirect to path or route name
     *
     * @param  string  $path
     * @param  boolean $isRoute
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    function redirect($path, $isRoute = true)
    {
        return new \Symfony\Component\HttpFoundation\RedirectResponse($path);
    }
}

if (!function_exists('sanitize')) {
    /**
     * sanitize param
     *
     * @param  mixed  $param
     * @param  boolean $debug
     * @return mixed
     */
    function sanitize($param, $debug = false)
    {
        if (!env('FORCE_SANITIZE_PARAM')) {
            return $param;
        }

        $original = $param;

        $sanitizeOutput = function ($final, $original) use ($debug) {
            if (!$debug) {
                return $final;
            }

            return array(
                'original' => $original,
                'final'    => $final,
            );
        };

        if (is_null($param) || empty($param) || is_bool($param)) {
            return $sanitizeOutput(
                $param,
                $original
            );
        }

        if (!is_scalar($param)) {
            throw new \Exception("only sanitize scalar variables!");
        }

        if (is_int($param)) {
            return $sanitizeOutput(
                (int) filter_var($param, FILTER_SANITIZE_NUMBER_INT),
                $original
            );
        }

        if (is_float($param)) {
            return $sanitizeOutput(
                (float) filter_var($param, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION),
                $original
            );
        }

        $param = trim($param);

        $param = stripslashes($param);

        $param = filter_var($param, FILTER_SANITIZE_STRING);

        return $sanitizeOutput($param, $original);
    }
}

if (!function_exists('sanitizeArray')) {
    /**
     * sanitize each members of array
     *
     * @param  array  &$array
     * @param  boolean $debug
     * @return void
     */
    function sanitizeArray(&$array, $debug = false)
    {
        array_walk_recursive($array, function (&$value) use ($debug) {
            $value = sanitize($value, $debug);
        });
    }
}

if (!function_exists("now")) {
    /**
     * get current timestamp
     *
     * @return \Carbon\Carbon
     */
    function now()
    {
        return \Carbon\Carbon::now()->timestamp;
    }
}

if (!function_exists("today")) {
    /**
     * get current human timestamp of today date
     *
     * @return \Carbon\Carbon
     */
    function today()
    {
        return \Carbon\Carbon::createFromTimestamp(now())->toDateTimeString();
    }
}

if (!function_exists("array_get")) {
    /**
     * get value in array by provided keys
     *
     * @param  array $array
     * @param  string $keys
     * @return mixed
     */
    function array_get(array $array, string $keys)
    {
        return \Arr::get($array, $keys);
    }
}

if (!function_exists("array_set")) {
    /**
     * set value in array with provided keys
     *
     * @param  array  $array
     * @param  string $keys
     * @param  mixed $value
     * @return mixed
     */
    function array_set(array $array, string $keys, $value)
    {
        return \Arr::set($array, $keys, $value);
    }
}

if (!function_exists("array_drop_number_keys")) {
    function array_drop_number_keys(array $array)
    {
        return \Arr::dropNumberKeys($array);
    }
}

if (!function_exists('assets')) {
    /**
     * get assets link
     *
     * @return string
     */
    function assets()
    {
        return new \Twig_SimpleFunction('assets', function ($path) {

            $complete = (baseUrl() . trim($path, "/"));

            if (substr($complete, -3) == 'css') {
                return '<link rel="stylesheet" href="' . $complete . '"/>';
            }

            if (substr($complete, -2) == 'js') {
                return '<script type="text/javascript" src="' . $complete . '"></script>';
            }

            return '';

        }, array('is_safe' => [true]));
    }
}

if (!function_exists('route')) {
    /**
     * get route by name
     *
     * @return string
     */
    function route()
    {
        $container = app()->getContainer();

        return new \Twig_SimpleFunction('route', function ($routeName, $method = null) use ($container) {
            if ($route = $container->get('routeCollection')->getRouteByName($routeName, $method)) {
                return baseUrl() . trim($route->getUri(), "/");
            }

            throw new \Exception("Non esiste nessuna rotta con questo nome!");
        });

    }
}

if (!function_exists('method_field')) {
    /**
     * set method to spoof
     *
     * @return string
     */
    function method_field()
    {
        $container = app()->getContainer();

        return new \Twig_SimpleFunction('method_field', function ($method) use ($container) {

            if (!in_array($method, ['PUT', 'PATCH', 'DELETE'])) {
                throw new \Exception("No allow method to spoof");
            }

            return '<input type="hidden" name="_method" value="' . $method . '" />';
        }, array('is_safe' => [true]));

    }
}

if (!function_exists('ddump')) {
    /**
     * set method to spoof
     *
     * @return string
     */
    function ddump()
    {
        $container = app()->getContainer();

        return new \Twig_SimpleFunction('ddump', function ($mixed) {
            return dump($mixed);
        }, array('is_safe' => [true]));

    }
}

if (!function_exists('auth')) {
    /**
     * set method to spoof
     *
     * @return string
     */
    function auth()
    {
        $container = app()->getContainer();

        return new \Twig_SimpleFunction('auth', function () use ($container) {
            return $container->auth->check();
        });

    }
}
