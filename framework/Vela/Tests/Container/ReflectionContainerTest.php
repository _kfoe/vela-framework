<?php declare (strict_types = 1);

namespace Vela\Tests;

use PHPUnit\Framework\TestCase;
use Vela\Container\Container;
use Vela\Container\Exception\NotFoundException;
use Vela\Container\ReflectionContainer;

class ReflectionContainerTest extends TestCase
{
    /**
     * Asserts that ReflectionContainer instantiates a class that does not have a constructor.
     */
    public function testContainerInstantiatesClassWithoutConstructor()
    {
        $classWithoutConstructor = \stdClass::class;

        $container = new ReflectionContainer;

        $this->assertInstanceOf($classWithoutConstructor, $container->get($classWithoutConstructor));
    }

    /**
     * Asserts that ReflectionContainer denies it has an item if a class does not exist for the alias.
     */
    public function testHasReturnsFalseIfClassDoesNotExist()
    {
        $container = new ReflectionContainer;

        $this->assertFalse($container->has('blah'));
    }

    /**
     * Asserts that ReflectionContainer claims it has an item if a class exists for the alias.
     */
    public function testHasReturnsTrueIfClassExists()
    {
        $container = new ReflectionContainer;

        $this->assertTrue($container->has(ReflectionContainer::class));
    }
}
