<?php declare (strict_types = 1);

namespace Vela\Tests;

use PHPUnit\Framework\TestCase;
use Vela\Container\Container;
use Vela\Container\Exception\NotFoundException;
use Vela\Tests\Acme\FooBar;
use Vela\Tests\Acme\FooFooBar;

class ContainerTest extends TestCase
{
    public function testContainerAddsAndGets()
    {
        $container = new Container;

        $container->add(FooBar::class);

        $this->assertTrue($container->has(FooBar::class));

        $foo = $container->get(FooBar::class);

        $this->assertInstanceOf(FooBar::class, $foo);
    }

    public function testContainerAddsAndGetsShared()
    {
        $container = new Container;

        $container->share(FooBar::class);

        $this->assertTrue($container->has(FooBar::class));

        $fooOne = $container->get(FooBar::class);

        $fooTwo = $container->get(FooBar::class);

        $this->assertInstanceOf(FooBar::class, $fooOne);

        $this->assertInstanceOf(FooBar::class, $fooTwo);

        $this->assertSame($fooOne, $fooTwo);
    }

    public function testContainerAddsAndGetsWithServiceProvider()
    {
        $container = new Container;

        $container->addServiceProvider(\Vela\ServiceProvider\ApplicationServiceProvider::class);

        $this->assertTrue($container->has(FooBar::class));

        $foo = $container->get(FooBar::class);

        $this->assertInstanceOf(FooBar::class, $foo);
    }

    public function testContainerDefinitionWithArguments()
    {
        $container = new Container;

        $container->add(FooFooBar::class)
            ->withArguments([
                'foo',
                'foobar',
            ]);

        $this->assertSame(
            $container->get(FooFooBar::class)->getA(),
            'foo'
        );

        $this->assertSame(
            $container->get(FooFooBar::class)->getB(),
            'foobar'
        );

        $container->add('testClosure', function (...$args) {
            return implode(',', $args);
        })
            ->withArguments([
                'foo',
                'foobar',
            ]);

        $this->assertSame(
            $container->get('testClosure'),
            'foo,foobar'
        );
    }

    // public function testContainerDefinitionWithArgumentsAndMethods()
    // {
    //     $container = new Container;

    //     $container->add(FooFooBar::class)
    //         ->withArguments([
    //             'foo',
    //             'foobar',
    //         ])
    //         ->withMethods([
    //             'changeA' => ['newCustomFooA']
    //         ]);

    //     $this->assertSame(
    //         $container->get(FooFooBar::class)->getA(),
    //         'newCustomFooA'
    //     );

    //     $this->assertSame(
    //         $container->get(FooFooBar::class)->getB(),
    //         'foobar'
    //     );
    // }

    // public function testContainerDefinitionWithMethods()
    // {
    //     $container = new Container;

    //     $definition = $container->add(FooFooBar::class);

    //     $this->assertSame(
    //         $container->get(FooFooBar::class)->getA(),
    //         null
    //     );

    //     $definition->withMethods([
    //         'changeA' => [false],
    //     ]);

    //     $this->assertSame(
    //         $container->get(FooFooBar::class)->getA(),
    //         "newA"
    //     );

    //     $definition->withMethods([
    //         'changeA' => ['newCustomA'],
    //     ]);

    //     $this->assertSame(
    //         $container->get(FooFooBar::class)->getA(),
    //         "newCustomA"
    //     );
    // }

    public function testContainerThrowsWhenCannotGetService()
    {
        $this->expectException(NotFoundException::class);

        $container = new Container;

        $this->assertFalse($container->has(Foo::class));

        $container->get(Foo::class);
    }
}