<?php declare (strict_types = 1);

namespace Vela\Tests\Acme;

use Vela\Tests\Acme\Bar;

class Foo
{
    public $f;

    public function __construct(Bar $f)
    {
        $this->f = $f;
    }
}
