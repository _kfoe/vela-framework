<?php declare (strict_types = 1);

namespace Vela\Tests\Acme;

class FooFooBar
{
    public $a;

    public $b;

    public function __construct($a = null, $b = null)
    {
        $this->a = $a;
        $this->b = $b;
    }

    public function changeA($customValue = false)
    {
        $this->a = (false === $customValue) ? "newA" : $customValue;
    }

    public function getA()
    {
        return $this->a;
    }

    public function getB()
    {
        return $this->b;
    }
}
