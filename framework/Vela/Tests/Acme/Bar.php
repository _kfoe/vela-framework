<?php declare (strict_types = 1);

namespace Vela\Tests\Acme;

use Vela\Tests\Acme\FooBar;

class Bar
{
    public function __construct(FooBar $r)
    {
        $this->r = $r;
    }
}
