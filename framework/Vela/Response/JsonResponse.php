<?php declare (strict_types = 1);

namespace Vela\Response;

use Symfony\Component\HttpFoundation\JsonResponse as SymfonyResponse;

/**
 * Response class
 */
class JsonResponse extends SymfonyResponse
{

}
