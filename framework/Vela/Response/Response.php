<?php declare(strict_types = 1);

namespace Vela\Response;

use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * Response class
 */
class Response extends SymfonyResponse
{
}
