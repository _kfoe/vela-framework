<?php declare (strict_types = 1);

namespace Vela\Session;

use Arr;
use Vela\Container\Container;

/**
 * Session class
 */
class Session
{
    /**
     * Container instance
     *
     * @var Vela\Container\Container
     */
    protected $container;

    /**
     * @var boolean
     */
    protected $isStarted = false;

    /**
     * Construct
     *
     * @param Vela\Container\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->boot();
    }

    /**
     * Close and destroy session
     *
     * @return void
     */
    public function close()
    {
        $params = session_get_cookie_params();

        session_unset();

        setcookie(
            array_get($this->container->get('config')->session, 'session_name'),
            '',
            (time() - 42000),
            array_get($params, 'path'),
            array_get($params, 'domain'),
            array_get($params, 'secure'),
            array_get($params, 'httponly')
        );

        session_destroy();
    }

    /**
     * Get item from session
     *
     * @param  string $key
     * @return mixed
     */
    public function get(string $key)
    {
        return Arr::get($_SESSION, $key);
    }

    /**
     * Check if session exists
     *
     * @param  string  $key
     * @return boolean
     */
    public function has(string $key)
    {
        return null !== $this->get($key);
    }

    /**
     * Set session with dot notation
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set(string $key, $value)
    {
        Arr::set([], $key, $value);
    }

    /**
     * Boot method
     *
     * @return void
     */
    private function boot()
    {
        if (!$this->isStarted && !isset($_SESSION)) {
            $this->configure();

            $this->isStarted = true;
        }
    }

    /**
     * Session configure
     *
     * @return void
     */
    private function configure()
    {
        ini_set(
            'session.cookie_lifetime',
            array_get($this->container->get('config')->session, 'cookie_lifetime')
        );

        ini_set(
            'session.cookie_path',
            array_get($this->container->get('config')->session, 'cookie_options.path')
        );

        ini_set(
            'session.cookie_domain',
            array_get($this->container->get('config')->session, 'cookie_options.domain')
        );

        ini_set(
            'session.cookie_secure',
            array_get($this->container->get('config')->session, 'cookie_options.secure')
        );

        ini_set(
            'session.cookie_httponly',
            array_get($this->container->get('config')->session, 'cookie_options.httponly')
        );

        session_set_cookie_params(
            array_get(session_get_cookie_params(), 'lifetime'),
            array_get(session_get_cookie_params(), 'path'),
            array_get(session_get_cookie_params(), 'domain'),
            array_get(session_get_cookie_params(), 'secure'),
            array_get(session_get_cookie_params(), 'httponly')
        );

        session_name(
            array_get($this->container->get('config')->session, 'session_name')
        );

        session_start();
    }
}
