<?php

namespace Vela\Session\LastActivity\Middleware;

use Closure;
use Vela\Application\BaseMiddleware\MiddlewareContract;
use Vela\Request\Request;
use Vela\Session\LastActivity\Contract\LastActivityContract;

/**
 * BaseLastActivityMiddleware middlware.
 */
abstract class BaseLastActivityMiddleware extends MiddlewareContract implements LastActivityContract
{
    /**
     * {@inheritdoc}
     */
    public function peel(Request $request, Closure $next)
    {
        if (in_array($request->getRoute()->getUri(), array_map(function ($v) {
            return trim($v, '/');
        }, $this->except))) {
            return $next($request);
        }

        if (!$this->checkLastActivity()) {
            return $this->expireLastActivity();
        }

        $this->setLastActivity();

        return $next($request);
    }
}
