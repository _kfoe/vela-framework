<?php declare (strict_types = 1);

namespace Vela\Session\LastActivity\Contract;

/**
 * LastActivityContract interface
 */
interface LastActivityContract
{
    /**
     * Verify last activity
     *
     * @return bool
     */
    public function checkLastActivity(): bool;

    /**
     * Callback that will be called if last activity is expired
     *
     * @return void
     */
    public function expireLastActivity();

    /**
     * Set last activity
     *
     * @return void
     */
    public function setLastActivity();
}
