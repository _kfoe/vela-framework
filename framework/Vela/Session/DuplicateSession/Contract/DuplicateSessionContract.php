<?php declare (strict_types = 1);

namespace Vela\Session\DuplicateSession\Contract;

/**
 * DulicateSessionContract interface
 */
interface DuplicateSessionContract
{
    /**
     * Verify if exists duplicate session
     *
     * @return bool
     */
    public function checkIfDuplicateSession(): bool;

    /**
     * Callback that will be called if exists dumplicate session
     *
     * @return void
     */
    public function whatNowWithDuplicateSession();
}
