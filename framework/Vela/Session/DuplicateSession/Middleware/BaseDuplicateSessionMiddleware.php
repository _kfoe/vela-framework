<?php

namespace Vela\Session\DuplicateSession\Middleware;

use Closure;
use Vela\Application\BaseMiddleware\MiddlewareContract;
use Vela\Request\Request;
use Vela\Session\DuplicateSession\Contract\DuplicateSessionContract;

/**
 * BaseDuplicateSessionMiddleware middlware.
 */
abstract class BaseDuplicateSessionMiddleware extends MiddlewareContract implements DuplicateSessionContract
{
    /**
     * {@inheritdoc}
     */
    public function peel(Request $request, Closure $next)
    {
        if (in_array($request->getRoute()->getUri(), array_map(function ($v) {
            return trim($v, '/');
        }, $this->except))) {
            return $next($request);
        }

        if ($this->checkIfDuplicateSession()) {
            return $this->whatNowWithDuplicateSession();
        }

        return $next($request);
    }
}
