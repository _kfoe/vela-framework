# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.23)
# Database: vela_gdr
# Generation Time: 2019-01-06 11:43:03 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table banned_users
# ------------------------------------------------------------

CREATE TABLE `banned_users` (
  `id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned DEFAULT NULL,
  `reason` varchar(255) NOT NULL,
  `ip` text,
  `banned_by` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `banned_by` (`banned_by`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bazar_bids
# ------------------------------------------------------------

CREATE TABLE `bazar_bids` (
  `id` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `price` decimal(7,2) unsigned NOT NULL,
  `last_bid` decimal(7,2) unsigned DEFAULT NULL,
  `sold_by` bigint(20) unsigned NOT NULL,
  `last_bid_by` bigint(20) unsigned DEFAULT NULL COMMENT 'id pg ultima puntata',
  `will_be_ended` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bazar_items
# ------------------------------------------------------------

CREATE TABLE `bazar_items` (
  `id` bigint(20) unsigned NOT NULL,
  `bazar_id` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `price` decimal(7,2) unsigned NOT NULL,
  `will_be_available` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bazar_id` (`bazar_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table bazars
# ------------------------------------------------------------

CREATE TABLE `bazars` (
  `id` bigint(20) unsigned NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `usable_by` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table chat
# ------------------------------------------------------------

CREATE TABLE `chat` (
  `id` bigint(20) unsigned NOT NULL,
  `map_chat_id` bigint(20) unsigned NOT NULL,
  `sender_id` bigint(20) unsigned NOT NULL,
  `receiver_id` bigint(20) unsigned DEFAULT NULL,
  `message` text NOT NULL,
  `tag` varchar(100) DEFAULT NULL,
  `type` char(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `map_chat_id` (`map_chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clan
# ------------------------------------------------------------

CREATE TABLE `clan` (
  `id` bigint(20) unsigned NOT NULL,
  `type_id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT 'standard_img.png',
  `description` text NOT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT '1',
  `type` bigint(20) unsigned NOT NULL,
  `usable_by` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clan_pg_roles
# ------------------------------------------------------------

CREATE TABLE `clan_pg_roles` (
  `id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `assigned_to` bigint(20) unsigned NOT NULL,
  `assigned_from` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clan_roles
# ------------------------------------------------------------

CREATE TABLE `clan_roles` (
  `id` bigint(20) unsigned NOT NULL,
  `clan_id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `icon` varchar(255) NOT NULL DEFAULT 'standard_img.png',
  `salary` decimal(7,2) unsigned NOT NULL DEFAULT '0.00',
  `is_boss` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table clan_types
# ------------------------------------------------------------

CREATE TABLE `clan_types` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table forum_types
# ------------------------------------------------------------

CREATE TABLE `forum_types` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table forums
# ------------------------------------------------------------

CREATE TABLE `forums` (
  `id` bigint(20) unsigned NOT NULL,
  `type_id` bigint(20) unsigned NOT NULL,
  `title` varchar(50) NOT NULL DEFAULT '',
  `usable_by` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table items
# ------------------------------------------------------------

CREATE TABLE `items` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type_id` bigint(20) unsigned NOT NULL COMMENT 'riferimento al bazar (mercato)',
  `builded_by` bigint(20) unsigned NOT NULL,
  `image` varchar(80) DEFAULT NULL,
  `usable_by` longtext,
  `assignable_slot` text COMMENT 'NULL = non assegnbabile, anche più di uno',
  `bonus_car0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `will_be_removed` tinyint(3) DEFAULT NULL COMMENT 'dopo quanti giorni dall acquisto scade (sigarette)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table logs
# ------------------------------------------------------------

CREATE TABLE `logs` (
  `id` bigint(20) unsigned NOT NULL,
  `code` varchar(80) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  `pg_id` bigint(20) unsigned DEFAULT NULL,
  `ip` varchar(255) NOT NULL,
  `info` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table map_chats
# ------------------------------------------------------------

CREATE TABLE `map_chats` (
  `id` bigint(20) unsigned NOT NULL,
  `world_map_id` bigint(20) unsigned DEFAULT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `thumb` varchar(255) NOT NULL DEFAULT '',
  `custom_fields` longtext COMMENT 'utilizzato per proprietà come orario d''apertura, chisura.. ecc ecc',
  `marker_x` smallint(4) unsigned NOT NULL DEFAULT '0',
  `marker_y` smallint(4) unsigned NOT NULL DEFAULT '0',
  `img_click` varchar(255) DEFAULT NULL,
  `goto_map` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table messages
# ------------------------------------------------------------

CREATE TABLE `messages` (
  `id` bigint(20) unsigned NOT NULL,
  `sender_id` bigint(20) unsigned NOT NULL,
  `receiver_id` bigint(20) unsigned NOT NULL,
  `message` text NOT NULL,
  `is_read` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pg
# ------------------------------------------------------------

CREATE TABLE `pg` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `surname` varchar(80) NOT NULL,
  `builded_by` bigint(20) unsigned NOT NULL,
  `sex_id` bigint(20) unsigned NOT NULL,
  `race_id` bigint(20) unsigned NOT NULL,
  `is_online` tinyint(1) NOT NULL DEFAULT '0',
  `pocket_money` decimal(7,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'soldi in tasca',
  `bank_money` decimal(7,2) unsigned NOT NULL DEFAULT '0.00',
  `is_hidden` tinyint(1) NOT NULL DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_logout` datetime DEFAULT NULL,
  `url_img_avatar` varchar(255) NOT NULL DEFAULT '../images/scheda/avatarbase.png',
  `url_img_chat` varchar(255) NOT NULL DEFAULT '../images/scheda/avatarbase.png',
  `exp` float(7,2) unsigned NOT NULL DEFAULT '0.00',
  `heal` smallint(11) unsigned DEFAULT NULL,
  `car0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `car1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `car2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `car3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `car4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `car5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `background` text,
  `last_activity` datetime NOT NULL,
  `last_salary` datetime NOT NULL,
  `is_administrated` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table pg_items
# ------------------------------------------------------------

CREATE TABLE `pg_items` (
  `id` bigint(20) unsigned NOT NULL,
  `assigned_to` bigint(20) unsigned NOT NULL,
  `item_id` bigint(20) unsigned NOT NULL,
  `comment` varchar(250) DEFAULT NULL,
  `will_be_removed` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` smallint(6) unsigned DEFAULT NULL,
  `shot` smallint(11) unsigned DEFAULT NULL COMMENT 'aka cariche',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='scade_il vale solo per gli oggetti che hanno scadenza';



# Dump of table races
# ------------------------------------------------------------

CREATE TABLE `races` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `is_in_signup` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `icon` varchar(255) NOT NULL DEFAULT 'standard_img.png',
  `bonus_car0` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car4` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car5` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table readed_threads
# ------------------------------------------------------------

CREATE TABLE `readed_threads` (
  `id` bigint(20) unsigned NOT NULL,
  `readed_by` bigint(20) unsigned NOT NULL,
  `thread_id` bigint(20) unsigned NOT NULL,
  `forum_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table sex_types
# ------------------------------------------------------------

CREATE TABLE `sex_types` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table skills
# ------------------------------------------------------------

CREATE TABLE `skills` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` text,
  `car_id` tinyint(3) unsigned NOT NULL,
  `usable_by` longtext,
  `bonus_car1` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car2` smallint(5) unsigned NOT NULL DEFAULT '0',
  `bonus_car3` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usable_after` longtext COMMENT 'dopo aver acquistato un oggetto, dopo un certo livello. ecc ecc...',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table threads
# ------------------------------------------------------------

CREATE TABLE `threads` (
  `id` bigint(20) unsigned NOT NULL,
  `parent_id` bigint(20) unsigned DEFAULT NULL,
  `forum_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `pg_id` bigint(20) unsigned NOT NULL,
  `is_important` tinyint(1) NOT NULL DEFAULT '0',
  `is_closed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_master` tinyint(1) NOT NULL DEFAULT '0',
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `is_mod` tinyint(1) NOT NULL DEFAULT '0',
  `session` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table where_is_pgs
# ------------------------------------------------------------

CREATE TABLE `where_is_pgs` (
  `id` bigint(20) unsigned NOT NULL,
  `pg_id` bigint(20) unsigned NOT NULL,
  `current_chat_id` bigint(20) unsigned DEFAULT NULL,
  `current_world_map_id` bigint(20) unsigned DEFAULT NULL,
  `assigned_world_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table work_pgs
# ------------------------------------------------------------

CREATE TABLE `work_pgs` (
  `id` bigint(20) unsigned NOT NULL,
  `work_id` bigint(20) unsigned NOT NULL,
  `assigned_to` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table works
# ------------------------------------------------------------

CREATE TABLE `works` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `salary` decimal(7,2) unsigned NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `usable_by` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table world_maps
# ------------------------------------------------------------

CREATE TABLE `world_maps` (
  `id` bigint(20) unsigned NOT NULL,
  `world_id` bigint(20) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `width` smallint(4) unsigned DEFAULT NULL,
  `height` smallint(4) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table world_object_types
# ------------------------------------------------------------

CREATE TABLE `world_object_types` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `world_object_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table world_objects
# ------------------------------------------------------------

CREATE TABLE `world_objects` (
  `id` bigint(20) unsigned NOT NULL,
  `type_id` bigint(20) unsigned NOT NULL,
  `world_id` bigint(20) unsigned DEFAULT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `model_type` varchar(80) NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table worlds
# ------------------------------------------------------------

CREATE TABLE `worlds` (
  `id` bigint(20) unsigned NOT NULL,
  `name` varchar(80) NOT NULL DEFAULT '',
  `world_map_id_start` bigint(20) unsigned NOT NULL COMMENT 'id mappa d''avvio per il mondo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
