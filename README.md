# Vela Framework

* MVC
* Facade system
* Symfony Request/Response
* Eloquent ORM
* Pipeline system
* Routing system with sanitize system (request and route params)
* Autowiring

# Introduzione
Questo framework è stato creato da zero al fine di comprendere le logiche fondamentali presenti all'interno dei framework Symfony & Laravel.

# TODO

*	decidere se usare come driver delle session il database
*	creare signup/login flusso completo
*	creare funzione redirect usabile da dentro controller -> chiama redirect helper.
*	trovare modo per dare l'opzione di escludere middleware dalle rotte