<?php

/**
 * Include the application init file.
 */
include dirname(__DIR__) . "/app/bootstrap.php";

/**
 * Start the application
 */
app()->run();
