<?php

Router::get('/', 'IndexController@index')->name('sss');
Router::get('/login', 'LoginController@login')->name('login');
Router::get('/logout', 'LoginController@logout')->name('logout');
Router::post('/login', 'LoginController@loginAction')->name('login.action');
