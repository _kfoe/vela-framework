<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * User model
 */
class User extends Model
{
    protected $guarded = ["id", "created_at", "updated_at"];
}
