{% extends 'base.twig.php' %}
{% block content %}
    <main role="main" class="container">
      <h1 class="mt-5">Vela Framework</h1>
      <br />
      {% if auth() %}
      	{{ ddump(user) }}
      {% endif %}
    </main>
{% endblock %}

