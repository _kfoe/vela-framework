{% block header %}
<html>
 <head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  {{assets("resource/assets/css/main.css", false)}}
 </head>
 <body>
   {% block content %}{% endblock %}

<footer class="footer">
      <div class="container">
        <span class="text-muted">
          {% if auth() %}
        	 <a href="{{route('logout')}}">Logout</a>
           {% else %}
            <a href="{{route('login')}}">Login</a>
           {% endif %}
        </span>
      </div>
    </footer>

  {{assets("resource/assets/js/vendor.js")}}
  {{assets("resource/assets/js/main.js")}}
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 </body>
</html>
{% endblock %}