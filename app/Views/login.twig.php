{% extends 'base.twig.php' %}
{% block content %}
<main role="main" class="container">
  <h1 class="mt-5">Vela Framework</h1>
  <br />
   <form method="POST" action="{{ route('login.action') }}">
    <div class="form-group">
     <label>
      Username
     </label>
     <input type="username" name="username" class="form-control" placeholder="Username" value="demo" />
    </div>
    <div class="form-group">
     <label>
      Password
     </label>
     <input type="password" name="password" class="form-control" placeholder="Password" value="demo"/>
    </div>
    <button type="submit" class="btn btn-primary">
     Accedi
    </button>
   </form>
</main>
{% endblock %}
