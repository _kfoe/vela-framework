<?php

namespace App\Http\Middleware;

use Vela\Security\CSRF\Middleware\BaseMiddleware;

/**
 * VerifyCsrfMiddleware middlware.
 */
class VerifyCsrfMiddleware extends BaseMiddleware
{
    /**
     * The URI that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [];
}
