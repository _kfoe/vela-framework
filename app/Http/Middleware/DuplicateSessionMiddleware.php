<?php

namespace App\Http\Middleware;

use Auth;
use BCrypt;
use Cookie;
use Vela\Session\DuplicateSession\Middleware\BaseDuplicateSessionMiddleware;

/**
 * DuplicateSessionMiddleware middlware.
 */
class DuplicateSessionMiddleware extends BaseDuplicateSessionMiddleware
{
    /**
     * The URI that should be excluded from last activity logic.
     *
     * @var array
     */
    protected $except = [];

    /**
     * {@inheritdoc}
     */
    public function checkIfDuplicateSession(): bool
    {
        if (!Auth::check()) {
            return false;
        }

        if (!BCrypt::verify(Cookie::get(array_get(app()->container->config->session, 'session_name')), Auth::user()->session)) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function whatNowWithDuplicateSession()
    {
        Auth::user()->update(['is_online' => 0]);

        Auth::logout();

        return app()->container->view->view('sessione.twig.php');
    }
}
