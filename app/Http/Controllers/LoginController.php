<?php

namespace App\Http\Controllers;

use Auth;
use Vela\Application\BaseController\BaseController;

/**
 * Login Controller
 */
class LoginController extends BaseController
{
    /**
     * GET /login
     *
     * @return \Vela\View\View
     */
    public function login()
    {
        return $this->render('login.twig.php');
    }

    /**
     * POST /login
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|string
     */
    public function loginAction()
    {
        if (Auth::attempt($this->request->username, $this->request->password)) {
            return redirect('/');
        }

        return "Errore durante l'inserimento dei dati";
    }

    /**
     * GET /logout
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logout()
    {
        if (Auth::check()) {
            Auth::logout();
        }

        return redirect('/');
    }
}
