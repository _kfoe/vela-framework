<?php

namespace App\Http\Controllers;

use Vela\Application\BaseController\BaseController;
use Auth;
/**
 * Demo controller
 */
class IndexController extends BaseController
{
    /**
     * GET /
     *
     * @return \Vela\View\View
     */
    public function index()
    {
        return $this->render('index.twig.php', [
        	'user' => Auth::user()
        ]);
    }
}
