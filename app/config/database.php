<?php

return [
    'driver'    => 'mysql',
    'host'      => env('HOST', 'localhost'),
    'database'  => env('DATABASE', 'my_database'),
    'username'  => env('USER_DB', 'root'),
    'password'  => env('PASS_DB', 'root'),
    'charset'   => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix'    => '',
];
