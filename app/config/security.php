<?php

return [
    #usato per verificare login.
    'SECRET' => env('SECRET'),

    'SECRET_ADMIN' => '1b$1NcsddåsAds4Idsfj',

    'SECRET_MOD' => '2b$14NcadsdsAds4Ij',

    'SECRET_MASTER' => '3b$2331NcsddddsAds4Ij',

    'BCRYPT_COST' => 10,
];
