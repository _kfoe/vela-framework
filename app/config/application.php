<?php

return [
    'timezone' => 'Europe/Rome',

    'charset' => 'UTF-8',

    'error_handler' => [
        'log_errors'     => true,
        'display_errors' => env('DEBUG', false),
    ],

    'middleware' => [
        Vela\Auth\Middleware\AuthenticateMiddleware::class,
        App\Http\Middleware\DuplicateSessionMiddleware::class,
        App\Http\Middleware\VerifyCsrfMiddleware::class,
    ],

    'providers' => [
        Vela\ServiceProvider\ApplicationServiceProvider::class,
        Vela\ServiceProvider\RoutingServiceProvider::class,
        Vela\ServiceProvider\DatabaseServiceProvider::class,
        Vela\ServiceProvider\ExceptionServiceProvider::class,
        Vela\ServiceProvider\SessionServiceProvider::class,
        Vela\ServiceProvider\CookieServiceProvider::class,
        Vela\ServiceProvider\SupportServiceProvider::class,
        Vela\ServiceProvider\ViewServiceProvider::class,
        Vela\ServiceProvider\SecurityServiceProvider::class,
        Vela\ServiceProvider\AuthServiceProvider::class,
    ],

    'aliases' => [
        'Router'  => Vela\Facade\RouterFacade::class,
        'Session' => Vela\Facade\SessionFacade::class,
        'Cookie'  => Vela\Facade\CookieFacade::class,
        'View'    => Vela\Facade\ViewFacade::class,
        'Arr'     => Vela\Facade\ArrFacade::class,
        'DB'      => Vela\Facade\DBFacade::class,
        'BCrypt'  => Vela\Facade\BCryptFacade::class,
        'Auth'    => Vela\Facade\AuthFacade::class,
    ],
];
