<?php

return [
    'session_name' => 'my_unique_name',

    'cookie_lifetime' => '0',

    'cookie_options' => [
        'path'     => '/',
        'domain'   => '',
        'secure'   => 'false',
        'httponly' => 'true',
    ],

    "SESSION_EXPIRE" => env('SESSION_EXPIRE_AFTER', 1),
];
