const config = require('./webpack.conf');
const _path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require('postcss-preset-env');
const cssnano = require('cssnano');
const clean = require('clean-webpack-plugin');
const C = (process.env.NODE_ENV === "production") ? config.build : config.dev

module.exports = {
  mode: C.mode,
  devtool: C.devtool,
  entry: ['babel-polyfill', require('path').resolve(__dirname, '../resource/assets/app.js')],
  output: C.output,
  devServer: config.dev.devServer,
  module: {
    rules: [{
      test: /\.js$/i,
      exclude: /node_modules/,
      use: {
        loader: "babel-loader",
        options: {
          plugins: [
            //transform-runtime
            "transform-object-rest-spread",
            "transform-async-to-generator",
          ],
          presets: [
            ["env", {
              modules: false
            }]
          ]
        }
      }
    }, {
      test: /\.(css|sass|scss)$/i,
      use: [
        'style-loader',
        MiniCssExtractPlugin.loader, {
          loader: 'css-loader',
          options: {
            importLoaders: 2,
            sourceMap: true
          }
        }, {
          loader: 'postcss-loader',
          options: {
            plugins: () => [
              postcssPresetEnv({
                browsers: ['last 4 versions']
              }),
              cssnano()
            ],
            sourceMap: true
          }
        }, {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ]
    }, {
      test: /\.(eot?.+|svg?.+|ttf?.+|otf?.+|woff?.+|woff2?.+)$/i,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 1000,
          name: "[path]/[hash].[ext]",
          outputPath: '/../../',
        }
      }]
    }, {
      test: /\.(png|gif|jpg|svg|jpeg)$/i,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 1000,
          name: "[path]/[hash].[ext]",
          outputPath: '/../../',
        }
      }]
    }]
  },
  optimization: {
    minimize: (process.env.NODE_ENV === "production") ? true : false,
    splitChunks: config.optimization.splitChunks
  },
  plugins: [
    new clean(['public/resource', 'public/_'], {
      root: _path.resolve(__dirname, '../'),
      verbose: true,
      dry: false,
      exclude: []
    }),
    new MiniCssExtractPlugin({
      filename: "resource/assets/css/[name].css",
    })
  ]
}