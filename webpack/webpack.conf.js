const _path = require('path');
module.exports = {
  optimization: {
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,
        vendor: {
          name: 'vendor',
          chunks: 'all',
          test: /node_modules/,
          priority: 20
        },
        common: {
          name: 'common',
          minChunks: 2,
          chunks: 'async',
          priority: 10,
          reuseExistingChunk: true,
          enforce: true
        }
      }
    }
  },
  build: {
    mode: "production",
    devtool: false,
    output: {
      path: _path.resolve(__dirname, "./../public"),
      filename: "resource/assets/js/[name].js",
      library: 'Vela'
    }
  },
  dev: {
    mode: "development",
    devtool: "cheap-module-eval-source-map",
    output: {
      path: _path.resolve(__dirname, "./../public"),
      filename: "resource/assets/js/[name].js",
      library: 'Vela',
    },
    devServer: {
      historyApiFallback: true,
      noInfo: false,
      inline: false,
      hot: false,
      port: 9000,
    }
  }
}