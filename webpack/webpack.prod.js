const baseWebpackConfig = require('./webpack.base')
const webpack = require('webpack');
const config = require('./webpack.conf');
var merge = require('webpack-merge');
const C = (process.env.NODE_ENV === "production") ? config.build : config.dev

module.exports = merge(baseWebpackConfig, {
  plugins: [
    new webpack.HashedModuleIdsPlugin()
  ]
})