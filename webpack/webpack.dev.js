const baseWebpackConfig = require('./webpack.base')
const webpack = require('webpack');
var merge = require('webpack-merge');
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');
const config = require('./webpack.conf');
const C = (process.env.NODE_ENV === "production") ? config.build : config.dev

module.exports = merge(baseWebpackConfig, {
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new FriendlyErrorsPlugin(),
    new LiveReloadPlugin({
      hostname: 'localhost'
    })
  ]
})