var objects = {
  foo: () => {
    console.log("foo")
    objects.foobar()
  },
  foobar: () => {
    console.log("foobar")
  }
}

export default objects;